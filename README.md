# invertible_networks_for_optimisation_of_pc

Repository for the paper "Fast,  efficient  and  flexible
particle  accelerator  optimisation using densely-connected and
invertible neural networks".

All ground truth calculations, i.e. data simulations, were performed with OPAL in the version `2.2.1`,
commit `4354d457cbd80fadd7e3587e75fbbbae89597198`.




# Installation

## Dependencies

We assume you install the code using Anaconda.
Please notice that you have to adapt the path
where your environment will be located.

```bash
conda_env_dir=path/to/your/new/env
```
**IMPORTANT:** All batch scripts in the repo assume that the environment is
located at `/data/user/bellotti_r/turning_tables_env`.


Then, in the same terminal, run the following commands:

```bash
conda create --prefix $conda_env_dir python=3.7
conda activate $conda_env_dir
# After typing the following command,
# you will have to confirm the installation.
conda install tensorflow==2.0.0 numpy==1.17.2 pandas==1.0.1 seaborn==0.10.1 scikit-learn==0.21.3
# Install plotly (needed for interactive visualisations).
conda install -c plotly plotly=4.9.0
pip3 install nbconvert==5.6.1
# Needed for the Jupyter notebooks.
conda install -c conda-forge ipykernel
# Install pymoo with hardware acceleration.
mkdir tmp
cd tmp
git clone https://github.com/msu-coinlab/pymoo
cd pymoo
git checkout 0.4.1
make compile
cd ../..
rm -rf tmp
# Install MLLIB, the machine learning framework developped at PSI.
pip3 install --user mllib@git+https://gitlab.psi.ch/adelmann/mllib.git@52798bbcb8abc9e87818ade3eac438590dafd222#egg=mllib
# For development of the models, the `ray` library provides a framework
# for parallel hyperparameter scans. To install it, run:
pip install --user ray[tune]==0.8.6
```

## Jupyter kernel

```bash
python -m ipykernel install --user --name turning_tables --display-name "Turning Tables"
```

## Repo
From within ``code/``, execute the following command:

```bash
pip3 install --user .
```


# Reproducing the results for AWA

## <span style="color:red">Project Directory Structure</span>

These are the most important directories and sub-directories in the project.

```txt
datasets/
code/
models/
    awa_forward_model/
    awa_invertible_model/
optimisation/
    awa/
predictions/
    awa_forward_model/
    awa_invertible_model/
```
The code assumes that this hierarchy is located at
`/data/user/bellotti_r/paper_new`. The code is written in a way that hopefully
makes it easy to change paths, either at  the beginning of postprocessing
Jupyter notebooks, or as command line arguments to Python scripts.


You should execute all Python scripts and notebooks using the environment
described above. We provide the commands as well as the SLURM scripts used to
generate the results of the paper. **Notice that you will have to adjust the
paths!** The paths are defined in two places:

- The JSON files where the model trainings are defined,
  see `code/experiment_configs/`.
  More precisely, you need to edit the `"save_dir"` value at the very bottom of
  the file. This file is referred to as `<save_dir>` in this README.
  Additionally, you need to adjust the paths of the datasets.
- The batch scripts themselves.

You should start the jobs from within the ``code/`` directory.

## Training and Evaluation

Before training a model, make sure that the directories `<save_dir>/logs` and
`<save_dir>/models` exist. `<save_dir>` is the directory that is given in the
corresponding experiment config JSON.

All commands in this section are to be run from within the ``code/`` directory.


#### Train models from paper
```bash
python3 scripts/train_network_script.py experiment_configs/awa_forward_model.json
python3 scripts/train_network_script.py experiment_configs/awa_invertible_model.json

# Alternative using SLURM:
# sbatch slurm_scripts/run_training_CPU_awa_best_forward_model.slurm
# sbatch slurm_scripts/run_training_CPU_awa_best_invertible_model.slurm
```

The forward model is saved at
`<save_dir>/models/hiddenLayers_7_unitsPerLayer_500_activation_relu_batch_size_256_learning_rate_0.0001_optimizer_adam_epochs_val_mean_absolute_percentage_error,20,1.,200_awa_best_forward_model`.
Rename this model to the more convenient name `awa_forward_model`
that is used in the post-processing
code.<br />
The invertible model is saved as `awa_best_invertible_model`.
Rename it to `awa_invertible_model`.

When the model is trained, we have to evaluate it on the test set.
That means, we take the design variables from the test set (which have not been
seen during training), predict the corresponding quantities of interest and
save them into the directories `<prediction_dir>/awa_forward_model` and
`<prediction_dir>/awa_invertible_model`, where `prediction_dir` is set in
`slurm_scripts/run_awa_forward_evaluation.slurm` or directly in the shell
commands below.

Run the following commands (don't forget to adapt the paths):

```bash
python3 scripts/forward_prediction_evaluation_script.py \
    awa_forward_model.json \
    awa_forward_model \
    --experiment_config_dir /data/user/bellotti_r/paper_new/invertible_network_paper/experiment_configs \
    --model_dir /data/user/bellotti_r/paper_new/models \
    --prediction_dir /data/user/bellotti_r/paper_new/predictions

python3 scripts/INN_evaluation_script.py --n_tries 32 \
    awa_invertible_model.json \
    awa_invertible_model\
    awa_forward_model \
    --experiment_config_dir /data/user/bellotti_r/paper_new/invertible_network_paper/experiment_configs \
    --model_dir /data/user/bellotti_r/paper_new/models \
    --prediction_dir /data/user/bellotti_r/paper_new/predictions

# Alternative using SLURM:
# sbatch slurm_scripts/run_awa_forward_evaluation.slurm
# sbatch slurm_scripts/run_awa_INN_evaluation.slurm
```

#### Run hyperparameter scan
In case you would like to run a hyperparameter scan for the invertible model,
first generate the configurations by running

```bash
python3 scripts/prepare_hyperparameter_scan_for_INN.py
```

The script contains paths you will need to adjust before running it.

Then launch the jobs with the following shell commands.
The commands assume that you are using the directory hierarchy described at the
top of this README, and that you start at the repo root.

```bash
for filename in ../hyperparameter_scans/awa/invertible_model/batch_scripts/*.slurm; do sbatch "$filename"; done
```

Afterward, you have to find out which parameter configuration is best.
This can be done by running
`postprocessing/analyse_hyperparameter_scan_awa_invertible_model.ipynb`.
Then copy the directory of the best model to the
models directory (rename the direcotry to `awa_invertible_model`
in order to make the scripts work), and the corresponding config file to
`experiment_configs/awa_invertible_model.json`.
Finally, you can run the evaluation scripts and run the plotting notebooks
(see above and below, respectively).




## Optimisation



To optimise the AWA, run the following scripts from within the ``code/``
directory:

```bash
python3 scripts/awa_optimisation.py \
    /data/user/bellotti_r/paper_new/models \
    /data/user/bellotti_r/paper_new/optimisation/awa/forward_model \
    1000 \
    200

python3 scripts/awa_optimisation.py \
    --use_invertible_surrogate \
    /data/user/bellotti_r/paper_new/models \
    /data/user/bellotti_r/paper_new/optimisation/awa/invertible_model \
    1000 \
    200

# Alternative using SLURM:
# sbatch slurm_scripts/optimise_awa_FW_model.slurm 
# sbatch slurm_scripts/optimise_awa_invertible_model.slurm
```

Once the optimisation is solved, we have to evaluate the
last generation with OPAL. For that, copy the TSV file containing
the non-dominated machine settings to the corresponding evaluation directory.
Assuming you are using the directory hierarchy described at the beginning of
this README, you can execute the following command from within the ``code/``
directory:

```bash
mkdir ../optimisation/awa/forward_model_validation
cp -R OPAL_configurations/awa/* ../optimisation/awa/forward_model_validation
python3 scripts/copy_DVARs_and_drop_duplicates.py ../optimisation/awa/forward_model/optimal_dvars.tsv ../optimisation/awa/forward_model_validation/optimal_dvars.tsv
cd ../optimisation/awa/forward_model_validation/
sbatch evaluate.slurm
```
You find an example slurm script and the additional files you need to operate OPAL in ``\code\OPAL_configurations\awa\``
Analogue for the optimisation that uses the invertible network:

```bash
mkdir ../optimisation/awa/invertible_model_validation
cp -R OPAL_configurations/awa/* ../optimisation/awa/invertible_model_validation
python3 scripts/copy_DVARs_and_drop_duplicates.py ../optimisation/awa/invertible_model/optimal_dvars.tsv ../optimisation/awa/invertible_model_validation/optimal_dvars.tsv
cd ../optimisation/awa/invertible_model_validation/
sbatch evaluate.slurm 
```

The next step is to extract the evaluated objectives by running the notebook
`postprocessing/extract_validated_points_awa.ipynb`.

Afterwards, one needs to extract all the non-dominated sets for different generations:

```bash
# Optimisation using forward model
for gen in {0..15..1}; do
    python3 scripts/awa_optimisation_extract_nondominated_sets.py \
        $gen \
        /data/user/bellotti_r/paper_new/optimisation/awa/forward_model \
        /data/user/bellotti_r/paper_new/optimisation/awa/forward_model/nondominated_set/gen_$gen.hdf5
done

for gen in {0..200..5}; do
    python3 scripts/awa_optimisation_extract_nondominated_sets.py \
        $gen \
        /data/user/bellotti_r/paper_new/optimisation/awa/forward_model \
        /data/user/bellotti_r/paper_new/optimisation/awa/forward_model/nondominated_set/gen_$gen.hdf5
done

for gen in {0..999..50}; do
    python3 scripts/awa_optimisation_extract_nondominated_sets.py \
        $gen \
        /data/user/bellotti_r/paper_new/optimisation/awa/forward_model \
        /data/user/bellotti_r/paper_new/optimisation/awa/forward_model/nondominated_set/gen_$gen.hdf5
done

python3 scripts/awa_optimisation_extract_nondominated_sets.py \
        999 \
        /data/user/bellotti_r/paper_new/optimisation/awa/forward_model \
        /data/user/bellotti_r/paper_new/optimisation/awa/forward_model/nondominated_set/gen_999.hdf5

##############################
# Optimisation using random initialisation.
for gen in {0..15..1}; do
    python3 scripts/awa_optimisation_extract_nondominated_sets.py \
        $gen \
        /data/user/bellotti_r/paper_new/optimisation/awa/invertible_model \
        /data/user/bellotti_r/paper_new/optimisation/awa/invertible_model/nondominated_set/gen_$gen.hdf5
done

for gen in {0..200..5}; do
    python3 scripts/awa_optimisation_extract_nondominated_sets.py \
        $gen \
        /data/user/bellotti_r/paper_new/optimisation/awa/invertible_model \
        /data/user/bellotti_r/paper_new/optimisation/awa/invertible_model/nondominated_set/gen_$gen.hdf5
done

for gen in {0..999..50}; do
    python3 scripts/awa_optimisation_extract_nondominated_sets.py \
        $gen \
        /data/user/bellotti_r/paper_new/optimisation/awa/invertible_model \
        /data/user/bellotti_r/paper_new/optimisation/awa/invertible_model/nondominated_set/gen_$gen.hdf5
done

python3 scripts/awa_optimisation_extract_nondominated_sets.py \
        999 \
        /data/user/bellotti_r/paper_new/optimisation/awa/invertible_model \
        /data/user/bellotti_r/paper_new/optimisation/awa/invertible_model/nondominated_set/gen_999.hdf5


##############################
# Alternative using SLURM:
# sbatch slurm_scripts/awa_optimisation_extract_non_dominated_sets_FW_model.slurm
# sbatch slurm_scripts/awa_optimisation_extract_non_dominated_sets_invertible_model.slurm
```

Due to a bug in OPAL, some runs hang randomly and must be restarted manually. Use the `postprocessing/fix_hanged_OPAL_runs.ipynb`
notebook to prepare the corresponding directories for re-evaluation.
It also prints the commands to be executed on the cluster.

Finally, visualise the results by running

```bash
python3 scripts/plot_awa_optimisation_results_script.py
```

and running the notebook ``postprocessing/plot_awa_optimisation.ipynb``.

# Reproducing the results for IsoDAR

## <span style="color:red">Project Directory Structure</span>

These are the most important directories and sub-directories in the project.

```txt
invertible_network_paper_data/
invertible_network_paper/
invertible_network_paper_results5000/
    models_30_11_2020/
        isodar/
            forward_model1
            invertible_model2
            models/
                isodar_forward_model
                isodar_invertible_model2
            optimisation_paper_7/
                forward_model
                invertible_model2
                forward_model_validation
```
The code assumes that this hierarchy is located at
`/data/user/boiger_r/invertible_network_paper`. The code is written in a way that hopefully
makes it easy to change paths, either at  the beginning of postprocessing
Jupyter notebooks, or as command line arguments to Python scripts.


All Python scripts and notebooks should be executed using the environment above. 
You have to adapt the batch scripts and the paths according to your project structure, 
especially in the following files:

- batch scripts (`slurm_scripts/isodar_scan_forward_model.slurm`, `slurm_scripts/isodar_scan_invertible_model.slurm`)
- Jupyter notebooks for postprocessing

## Training and Evaluation

First run the batch script for the hyperparameter scan of the forward model. Run it from within the folder, where the code is located. 

```bash
python3 scripts/Isodar_hyperparameter_scan_model.py --datafile /data/user/boiger_r/invertible_network_paper_data/data_sonali/samples_5000_short.hdf5 --result_dir /data/user/boiger_r/invertible_network_paper_results5000/models_30_11_2020/isodar/forward_model1 --model forward

# Alternative using SLURM:
# sbatch slurm_scripts/isodar_scan_forward_model.slurm
```
If you only want to run one model, please adapt the hyperparameters in `scripts/Isodar_hyperparameter_scan_model.py` accordingly. 

Once the training has finished, choose the best forward model by creating the performance plots and comparing the measures (R2 - values). This can be done with the notebook:
`postprocessing/Isodar_analyse_hyperparameter_scan_forward_model.ipynb`
The best model will be stored separately. 
By running the notebook `postprocessing/Isodar_forward_model_info.ipynb`,
the best model is evaluated with the test data to see the performance of the forward model for data it has not seen before.  

The same procedure has to be repeated for the invertible model. 
First run the hyperparameter scan:

```bash
python3 scripts/Isodar_hyperparameter_scan_model.py --datafile /data/user/boiger_r/invertible_network_paper_data/data_sonali/samples_5000_short.hdf5 --result_dir /data/user/boiger_r/invertible_network_paper_results5000/models_30_11_2020/isodar/invertible_model2 --model invertible

# Alternative using SLURM:
# sbatch slurm_scripts/Isodar_scan_invertible_model.slurm
```
Find the best model by running the notebook
`postprocessing/Isodar_analyse_hyperparameter_scan_invertible_model.ipynb` and
evaluate the model performance of the best invertible model by running the
notebook `postprocessing/Isodar_invertible_model.ipynb`.


## Optimisation

To perform the IsoDAR optimisation, run the notebook
`postprocessing/ Isodar_optimization_ND.ipynb`.
Set the objectives you want to optimize and choose the constraints and set the upper 
and lower bounds. The optimization is then done automatically with the forward model, 
first using random initialization and second using the invertible model for the 
biased initialization. For the biased initialization one solution of the pareto front
of generation 999 is chosen (7) and used to compute 300 new starting configurations with 
the invertible model. 
To validate the results with OPAL, choose some points (dvar) of the Pareto fronts.
This can be done with the notebook
`postprocessing/ Isodar_optimization_opal_validation.ipynb`.
Use OPAL to compute the according qoi.
(How to operate OPAL, can be found in the according documentation of OPAL).
The necessary files for that can be found in ``code/OPAL_configurations/isodar/``.

Afterwards, you can generate the plots showing the results for the optimization
by running the notebook
`postprocessing/Isodar_optimization_plots_ND.ipynb`.

The speedup calculation for AWA and IsoDAR can be found in the notebook
`postprocessing/Isodar_speedup.ipynb`.
