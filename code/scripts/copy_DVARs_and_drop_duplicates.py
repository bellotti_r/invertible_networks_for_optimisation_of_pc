import argparse
import pandas as pd

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Copy the optimal DVAR file (while dropping duplicate rows) to another directory.')

    parser.add_argument('in_file')
    parser.add_argument('out_file')
    args = parser.parse_args()

    dvar = pd.read_csv(args.in_file, sep='\t')
    dvar = dvar.drop_duplicates()
    dvar.to_csv(args.out_file, sep='\t', index=False)
