import argparse
import os
import time
import numpy as np
import pandas as pd
import tensorflow as tf
tf.keras.backend.set_floatx('float64')
from mllib.model import KerasSurrogate
from bellotti_r_master_thesis.invertible_network.invertible_neural_network import InvertibleNetworkSurrogate
from bellotti_r_master_thesis.optimisation_helpers import OptimisationRunner

# Reproducibility:
# Need defined RNG state for sampling the initial generation with the
# invertible neural network model.
SEED = 498882
tf.random.set_seed(SEED)
np.random.seed(SEED)


target_for_initialisation = pd.DataFrame(data=[{
    'Mean Bunch Energy': 47.5,
    'RMS Beamsize in x': 2 * 1e-3,
    'RMS Beamsize in y': 2 * 1e-3,
    'Normalized Emittance x': 3 * 1e-6,
    'Normalized Emittance y': 3 * 1e-6,
    'energy spread of the beam': 60 * 1e-3,
    'Path length': 13.7,
}])


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Optimise the AWA using surrogate models.')
    parser.add_argument('model_dir',
                        help='Directory where the models are saved.')
    parser.add_argument('result_dir',
                        help='Directory where the results will be saved.')
    parser.add_argument('n_gen',
                        type=int,
                        help='Number of generations for NSGA II.')
    parser.add_argument('n_individuals',
                        type=int,
                        help='Number of individuals for NSGA II.')
    parser.add_argument('--use_invertible_surrogate',
                        help='Whether to use the invertible model to initialise the optimisation.',
                        action='store_true')
    args = parser.parse_args()

    # Config.
    model_dir = args.model_dir
    use_invertible_surrogate = args.use_invertible_surrogate
    n_gen = args.n_gen
    n_individuals = args.n_individuals

    result_dir = args.result_dir
    if not os.path.exists(result_dir):
        os.makedirs(result_dir)

    optimisation_name = 'awa_optimisation'
    if use_invertible_surrogate:
        optimisation_name += '_FW_model'
    else:
        optimisation_name += '_invertible_model'

    model_name = 'awa_forward_model'
    invertible_model_name = 'awa_invertible_model'

    start_time = time.time()

    # Load the model(s).
    surr = KerasSurrogate.load(model_dir, model_name)
    invertible_surrogate = None
    if use_invertible_surrogate:
        invertible_surrogate = InvertibleNetworkSurrogate.load(model_dir, invertible_model_name)

    # Optimise.
    optimisation = OptimisationRunner('awa',
                                      surr,
                                      inv_surr=invertible_surrogate,
                                      target_for_INN=target_for_initialisation)
    res = optimisation.solve(n_gen=n_gen, n_individuals=n_individuals)
    result = optimisation.extract_results(res)
    all_dvars, all_objectives, all_constraint_violations, optimal_dvars, optimal_objectives, feasible = result.values()

    end_time = time.time()

    # Log time-to-solution.
    duration = end_time - start_time

    print(f'Execution time of the simulation: {duration}s')

    with open(f'{result_dir}/execution_time.txt', 'w') as file:
        file.write(f'{duration} s')

    with open(f'{result_dir}/feasible.txt', 'w') as file:
        feasible = 'True' if feasible else 'False'
        file.write(feasible)

    optimisation.save_results(result_dir, result)
