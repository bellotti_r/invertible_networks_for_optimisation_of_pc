import os
import argparse
import numpy as np
import pandas as pd
import tensorflow as tf
from mllib.model import KerasSurrogate
from bellotti_r_master_thesis.experiment_config import ExperimentConfig
from bellotti_r_master_thesis.invertible_network.invertible_neural_network import InvertibleNetworkSurrogate

np.random.seed(43893053)
tf.keras.backend.set_floatx('float64')

if __name__ == '__main__':
    # parse the command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('experiment_config_file',
                        help='Filename of the experiment config file to use.')
    parser.add_argument('model_name',
                        help='Name of the invertible surrogate model to evaluate.')
    parser.add_argument('--experiment_config_dir',
                        help=('Directory containing experiment config files.\n'
                              'Default: ~/$USER/experiment_configs'),
                        default=f'{os.path.expanduser("~")}/{os.environ["USER"]}/experiment_configs')
    parser.add_argument('--model_dir',
                        help=('Directory where the models are stored.\n'
                              'Default: /data/user/$USER/models'),
                        default=f'/data/user/{os.environ["USER"]}/models')
    parser.add_argument('--prediction_dir',
                        help=('Directory to store the predicted values in.\n'
                              'Default: /data/user/$USER/datasets/predictions'),
                        default=f'/data/user/{os.environ["USER"]}/datasets/predictions')
    parser.add_argument('--evaluate_training_set',
                        help='Flag to evaluate the the model on the training set as well',
                        action='store_true')

    args = parser.parse_args()
    print(args)

    qoi_labels = [
        'Mean Bunch Energy',
        'RMS Beamsize in x',
        'RMS Beamsize in y',
        'Normalized Emittance x',
        'Normalized Emittance y',
        'energy spread of the beam',
        'Path length',
    ]

    evaluate_training_set = args.evaluate_training_set

    # paths to save the output to
    prediction_dir = f'{args.prediction_dir}/{args.model_name}'

    if not os.path.exists(prediction_dir):
        os.makedirs(prediction_dir)

    test_prediction_file = f'{prediction_dir}/forward_prediction_qois_test.hdf5'
    train_prediction_file = f'{prediction_dir}/forward_prediction_qois_train.hdf5'

    # load the experiment config, the surrogate models and the dataset
    config = ExperimentConfig(f'{args.experiment_config_dir}/{args.experiment_config_file}')

    if config.model_type == 'dense':
        surr = KerasSurrogate.load(args.model_dir, args.model_name)
    elif config.model_type == 'invertible':
        surr = InvertibleNetworkSurrogate.load(args.model_dir, args.model_name)
    else:
        raise NotImplementedError

    dvar_test, qoi_test = config.test_data()

    # predict
    qoi_pred_test = surr.predict(dvar_test.drop(columns='sample_id').reset_index(drop=True).values, batch_size=config.model_parameters['batch_size'])
    if config.model_type == 'dense':
        qoi_pred_test = pd.DataFrame(data=qoi_pred_test, columns=qoi_test.columns)
    elif config.model_type == 'invertible':
        # remove the path length
        qoi_pred_test = pd.DataFrame(data=qoi_pred_test[:, :-1], columns=qoi_test.columns)
    else:
        raise NotImplementedError

    qoi_pred_test.to_hdf(test_prediction_file, key='qoi')

    if evaluate_training_set:
        dvar_train, qoi_train = config.training_data()
        qoi_pred_train = surr.predict(dvar_train.drop(columns='sample_id').reset_index(drop=True).values)

        if config.model_type == 'dense':
            qoi_pred_train = pd.DataFrame(data=qoi_pred_train,
                                          columns=qoi_train.columns)
        elif config.model_type == 'invertible':
            # remove the path length
            qoi_pred_train = pd.DataFrame(data=qoi_pred_train[:, :-1],
                                          columns=qoi_test.columns)
        else:
            raise NotImplementedError

        qoi_pred_train.to_hdf(train_prediction_file, key='qoi')
