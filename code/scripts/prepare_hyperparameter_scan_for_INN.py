import os
import json
import copy
from itertools import product
import numpy as np
import pandas as pd


if __name__ == '__main__':
    scan_name = 'invertible_model'
    save_dir = f'/data/user/bellotti_r/paper_new/hyperparameter_scans/awa/{scan_name}'
    cores_per_job = 12

    if not os.path.exists(save_dir):
        os.makedirs(save_dir)

    # create the log directory
    log_dir = f'{save_dir}/logs'
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)

    # path to the SLURM batch template
    # It contains 3 placeholders:
    # One for number of CPU cores to use for each parameter configuration.
    # One for the  job name.
    # One for the path to the experiment config file to run.
    slurm_batch_script_template_file = '/data/user/bellotti_r/paper_new/invertible_network_paper/slurm_scripts/run_parameter_config.slurm'

    # None values in the following dict indicate that the value is not
    # scanned as an independent QOI, but depends on a scanned parameter
    # and is calculated.
    # Ex.: nominal_dim depends on z-dim.
    # In the dict, it is set to None to get a better overview over the parameters.
    fix_parameters = {
        'experiment_name': 'INN_lambda_gun_only_quantile_transformer_3x3x40',
        'training_dataset_name': '4_peak_distr_lower_charge_gun_and_cavities_20k slinear 2D_filtered_filtered_epsilon_clipped_at_0.0002_no_solenoids2_no_cavities',
        'training_data_paths': ['/data/user/bellotti_r/paper_new/datasets/4_peak_distr_lower_charge_gun_and_cavities_20k slinear 2D_filtered_filtered_epsilon_clipped_at_0.0002_no_solenoids2_no_cavities.hdf5'],
        'test_dataset_name': '4_peak_distr_lower_charge_gun_and_cavities_1k slinear 2D_filtered',
        'test_data_paths': ['/data/user/bellotti_r/paper_new/datasets/4_peak_distr_lower_charge_gun_and_cavities_1k slinear 2D_filtered.hdf5'],
        'predicted_qois': [
            'Mean Bunch Energy',
            'RMS Beamsize in x',
            'RMS Beamsize in y',
            'Normalized Emittance x',
            'Normalized Emittance y',
            'energy spread of the beam'
        ],
        'dvar_bounds': {
            'IBF': [450.0, 550.0],
            'IM': [100.0, 260.0],
            'GPHASE': [-50.0, 10.0],
            'ILS1': [0, 250],
            'ILS2': [0, 200],
            'ILS3': [0, 200],
            'bunchCharge': [0.3, 5],
            'lambda': [0.3, 2],
            'SIGXY': [1.5, 12.5],
            'Path length': [0.0, 13.7]
        },
        'qoi_bounds': {
            'Mean Bunch Energy': [0.0, 50],
            'RMS Beamsize in x': [0.0, 0.015],
            'RMS Beamsize in y': [0.0, 0.015],
            'Normalized Emittance x': [0.0, 0.0002],
            'Normalized Emittance y': [0.0, 0.0002],
            'energy spread of the beam': [0.0, 1.0],
            'Path length': [0.0, 13.7]
        },
        'model_type': 'invertible',
        'model_parameters': {
            'validation_model': [
			    "/data/user/bellotti_r/paper_new/models",
			    "awa_forward_model"
		    ],
            'num_dvars_to_average_for_validation': 1,
            'x_dim': 10,
            'y_dim': 7,
            'z_dim': 1,
            'nominal_dim': 12,
            'coefficient_net_activations': None,
            'sampling_distribution': 'uniform',
            'loss_weight_artificial': 1.0,
            'loss_weight_reconstruction': 3.0,
            'optimizer': 'adam',
            'learning_rate': 0.0001,
            'epochs': 15,
            'batch_size': 256,
        },
        'save_dir': save_dir
    }

    parameters_to_scan = {
        'number_of_blocks': [4, 6, 8, 10, 12],
        'depth': [3, 5, 7],
        'width': [60, 80, 100, 120, 140],
        'loss_weight_x': [400],
        'loss_weight_y': [400],
        'loss_weight_z': [400],
    }


    parameters = []

    config_dir = f'{save_dir}/configurations'
    if not os.path.exists(config_dir):
        os.makedirs(config_dir)

    # write the batch script
    batch_dir = f'{save_dir}/batch_scripts'
    if not os.path.exists(batch_dir):
        os.makedirs(batch_dir)

    # load the template
    with open(slurm_batch_script_template_file, 'r') as file:
        template = file.read()

    # create the parameter configs,  and the batch scripts
    for param_ID, parameter_values in enumerate(product(*parameters_to_scan.values())):
        parameter_values = dict(zip(parameters_to_scan.keys(),
                                    parameter_values))

        last_units = fix_parameters['model_parameters']['nominal_dim'] // 2
        depth = parameter_values['depth']
        width = parameter_values['width']
        activation_config = ['relu']*depth + ['linear']
        unit_config = [width]*depth + [last_units]

        del parameter_values['depth']
        del parameter_values['width']
        parameter_values['coefficient_net_activations'] = activation_config
        parameter_values['coefficient_net_units'] = unit_config

        parameters.append(parameter_values)

        # create the experiment config
        config = copy.deepcopy(fix_parameters)
        config['experiment_name'] = f'{scan_name}_{param_ID}'
        for key in parameter_values.keys():
            config['model_parameters'][key] = parameter_values[key]

        # write the experiment config file to disk (JSON)
        json_path = f'{config_dir}/{param_ID}.json'
        with open(json_path, 'w') as file:
            json.dump(config, file, indent='\t')

        # create and write the batch script
        job_name = f'{scan_name}_{param_ID}'
        batch_script = template.format(cores_per_job, job_name, json_path)
        with open(f'{batch_dir}/{param_ID}.slurm', 'w') as file:
            file.write(batch_script)

    # log all the parameter values to a CSV file
    keys = parameters[0].keys()
    to_write = []
    for param in parameters:
        param['coefficient_net_units'] = str(param['coefficient_net_units'])
        to_write.append(np.array(list(param.values())))
    to_write = np.vstack(to_write)
    print(to_write)
    parameters = pd.DataFrame(data=to_write, columns=keys)
    parameters.to_csv(f'{config_dir}/parameters.csv')
    print(parameters)
