import os
import time
import argparse
import ray
from ray import tune
from bellotti_r_master_thesis.isodar_scan_helper_functions import train_isodar_forward_model, train_isodar_invertible_model

if __name__ == '__main__':
    # Parse the command line arguments.
    parser = argparse.ArgumentParser()
    parser.add_argument('--result_dir',
                        help='Directory in which to save the results.')
    parser.add_argument('--model',
                        help='For which model to scan the parameters; can be "forward" or "invertible" (without the quotation marks)')
    parser.add_argument('--datafile',
                        help='Path to the HDF5 file containing the dataset.')
    args = parser.parse_args()

    result_dir = args.result_dir
    history_dir = f'{result_dir}/histories'

    for directory in [result_dir, history_dir]:
        if not os.path.exists(directory):
            os.makedirs(directory)

    train_func = None
    # change the following hyperparameters according to your problem, 
    # when using the invertible model, make sure to have a forward model
    # available and the change the path to that in the script
    # belotti_r_master_thesis/isodar_scan_helper_functions.py at 
    # function train_isodar_invertible_model, line 246
    if args.model == 'forward':
        train_func = train_isodar_forward_model
        config = {
            'width': tune.grid_search([10, 20, 40, 80, 160]),
            'depth': tune.grid_search([4, 5, 6, 7]),
            'lr': tune.grid_search([0.001, 0.0005, 0.0001]),
            'batch_size': tune.grid_search([256, 512]),
            'datafile': args.datafile,
        }
    elif args.model == 'invertible':
        train_func = train_isodar_invertible_model
        config = {
           'n_blocks': tune.grid_search([3,4,5,6,7]),
            'n_depth': tune.grid_search([1,2, 3]),
            'n_width': tune.grid_search([40, 50,60,70,80]),
            'learning_rate': tune.grid_search([ 0.05, 0.001, 0.005]),
            'batch_size': tune.grid_search([4,8,10,12,16,32,64]),
            'datafile': args.datafile,
        }
    else:
        raise ValueError(f'Unknown model: {args.model}')

    ray.init()

    start = time.time()
    # change the resources_per_trial and num_samples depending on the machine, 
    # where you run the code and on the hyperparameter_configs
    analysis = tune.run(train_func,
                        config=config,
                        local_dir=result_dir,
                        num_samples=1,
                        resources_per_trial={'cpu': 1},
                        queue_trials=False)

    # Save the training histories and the results.
    for key, df in analysis.trial_dataframes.items():
        ID = df['trial_id'].unique()[0]
        df.to_csv(f'{history_dir}/{ID}.csv')

    analysis.dataframe().to_csv(f'{result_dir}/results.csv')

    end = time.time()
    with open(f'{result_dir}/time_for_FW_scan.txt', 'w') as file:
        file.write(f'{end - start} s')
