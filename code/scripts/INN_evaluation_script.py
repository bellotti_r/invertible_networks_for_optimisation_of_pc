import os
import argparse
import numpy as np
import pandas as pd
import tensorflow as tf
from mllib.model import KerasSurrogate
from bellotti_r_master_thesis.experiment_config import ExperimentConfig
from bellotti_r_master_thesis.invertible_network.invertible_neural_network import InvertibleNetworkSurrogate

SEED = 43893053
np.random.seed(43893053)
tf.random.set_seed(SEED)
tf.keras.backend.set_floatx('float64')

if __name__ == '__main__':
    # parse the command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('experiment_config_file',
                        help='Filename of the experiment config file to use.')
    parser.add_argument('model_name',
                        help='Name of the invertible surrogate model to evaluate.')
    parser.add_argument('val_model_name',
                        help='Name of the validation model (forward model)')
    parser.add_argument('--experiment_config_dir',
                        help=('Directory containing experiment config files.\n'
                              'Default: ~/$USER/experiment_configs'),
                        default=f'{os.path.expanduser("~")}/{os.environ["USER"]}/experiment_configs')
    parser.add_argument('--model_dir',
                        help=('Directory where the models are stored.\n'
                              'Default: /data/user/$USER/models'),
                        default=f'/data/user/{os.environ["USER"]}/models')
    parser.add_argument('--prediction_dir',
                        help=('Directory to store the predicted values in.\n'
                              'Default: /data/user/$USER/datasets/predictions/<model_name>'),
                        default=None)
    parser.add_argument('--n_tries',
                        type=int,
                        help=('How many DVAR configs to try before taking the best one.\n'
                              'Default: 4'),
                        default=4)
    parser.add_argument('--last_s_only',
                        type=bool,
                        help=('Whether to evaluate at all longitudinal positions (False) or just at the end (True).\n'
                              'Default: False'),
                        default=False)

    args = parser.parse_args()
    print(args)

    qoi_labels = [
        'Mean Bunch Energy',
        'RMS Beamsize in x',
        'RMS Beamsize in y',
        'Normalized Emittance x',
        'Normalized Emittance y',
        'energy spread of the beam',
        'Path length',
    ]

    # paths to save the output to
    if args.prediction_dir is None:
        prediction_dir = f'/data/user/{os.environ["USER"]}/datasets/predictions/{args.model_name}'
    else:
        prediction_dir = f'{args.prediction_dir}/{args.model_name}'

    if args.last_s_only:
        prediction_dir += '_last_s_only'

    if not os.path.exists(prediction_dir):
        os.makedirs(prediction_dir)

    n_tries = args.n_tries

    prediction_file = f'{prediction_dir}/sampled_and_evaluated_qois_n_tries_{n_tries}.hdf5'
    prediction_INN_file = f'{prediction_dir}/sampled_and_evaluated_qois_INN_n_tries_{n_tries}.hdf5'
    sampled_DVARs_file = f'{prediction_dir}/sampled_DVARs_n_tries_{n_tries}.hdf5'

    # load the experiment config, the surrogate models and the dataset
    config = ExperimentConfig(f'{args.experiment_config_dir}/{args.experiment_config_file}')

    invertible_surr = InvertibleNetworkSurrogate.load(args.model_dir, args.model_name)
    val_surr = KerasSurrogate.load(args.model_dir, args.val_model_name)

    dvar_test, qoi_test = config.test_data()

    dvar_labels = dvar_test.drop(columns='sample_id').columns

    qoi_test['Path length'] = dvar_test['Path length']
    # make sure the QOI are in the right order!
    qoi_test = qoi_test[qoi_labels]

    if args.last_s_only:
        s = qoi_test['Path length'].unique()
        qoi_test = qoi_test[qoi_test['Path length'] == s[-1]]

    desired_qois = qoi_test.reset_index(drop=True)

    # sample
    if n_tries == 1:
        DVARs = invertible_surr.sample(desired_qois.values, batch_size=256)
    else:
        DVARs = invertible_surr.sample_n_tries(desired_qois.values,
                                               batch_size=256,
                                               n_tries=n_tries)

    DVARs = pd.DataFrame(data=DVARs, columns=dvar_labels).drop(columns='Path length')

    DVARs_to_eval = DVARs
    DVARs_to_eval['Path length'] = desired_qois['Path length']

    # guarantee column order
    DVARs_to_eval = DVARs_to_eval[['IBF',
                                   'IM',
                                   'GPHASE',
                                   'ILS1',
                                   'ILS2',
                                   'ILS3',
                                   'bunchCharge',
                                   'lambda',
                                   'SIGXY',
                                   'Path length']]

    # forward prediction with the validation model
    evaluated_qois = val_surr.predict(DVARs_to_eval.values)
    evaluated_qois = pd.DataFrame(data=evaluated_qois,
                                  columns=['Mean Bunch Energy',
                                           'RMS Beamsize in x',
                                           'RMS Beamsize in y',
                                           'Normalized Emittance x',
                                           'Normalized Emittance y',
                                           'energy spread of the beam',
                                           'Correlation xpx',
                                           'Correlation ypy'])

    # save the predictions
    DVARs.to_hdf(sampled_DVARs_file, key='dvar')
    DVARs_to_eval.to_hdf(prediction_file, key='dvar')
    evaluated_qois.to_hdf(prediction_file, key='qoi')

    # forward prediction with the invertible model
    predicted_INN = invertible_surr.predict(DVARs_to_eval, batch_size=256)
    predicted_INN = pd.DataFrame(data=predicted_INN[:, :-1],     # remove the path length
                                 columns=['Mean Bunch Energy',
                                          'RMS Beamsize in x',
                                          'RMS Beamsize in y',
                                          'Normalized Emittance x',
                                          'Normalized Emittance y',
                                          'energy spread of the beam'])

    # save the predictions
    DVARs_to_eval.to_hdf(prediction_file, key='dvar')
    predicted_INN.to_hdf(prediction_INN_file, key='qoi')
