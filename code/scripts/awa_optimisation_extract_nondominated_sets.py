import argparse
import os
import sys
import numpy as np
import pandas as pd
from pymoo.util.nds.non_dominated_sorting import NonDominatedSorting

if __name__ == '__main__':
    # Parse the arguments.
    parser = argparse.ArgumentParser(description='Extract the non-dominated set up to and including a given generation.')

    parser.add_argument('generation',
                        type=int,
                        help='Generation up to which (including) to extract feasible the non-dominated set.')
    parser.add_argument('optimisation_dir',
                        help='Directory where the results of the optimisation are saved.')
    parser.add_argument('output_file',
                        help='File which will contain the DataFrames of the non-dominated DVARs (key: dvar) and objectives (key: obj)')

    args = parser.parse_args()

    out_path = os.path.abspath(args.output_file)
    out_dir = os.path.dirname(out_path)

    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    objectives = [
        'RMS Beamsize in x',
        'Normalized Emittance x',
        'energy spread of the beam'
    ]

    # Load the optimisation results.
    d = args.optimisation_dir
    dvar_generations = pd.read_hdf(f'{d}/dvar_generations.hdf5')
    obj_generations = pd.read_hdf(f'{d}/objective_generations.hdf5')
    cv = np.load(f'{d}/constraint_violations.npy')
    feasible = np.isclose(cv, 0.)

    dvar = dvar_generations.query(f'`generation` <= {args.generation}').copy()
    obj = obj_generations.query(f'`generation` <= {args.generation}').copy()

    # Calculate the feasible set.
    obj['feasible'] = False

    for gen, df in obj.groupby('generation'):
        obj.loc[df.index, 'feasible'] = feasible[gen, :]

    # Calculate the non-dominated feasible set.
    nds = NonDominatedSorting()

    feasible_obj = obj.query('`feasible`').copy()
    feasible_dvar = dvar.loc[feasible_obj.index].copy()

    if feasible_obj.shape[0] == 0:
        sys.exit('infeasible')

    nondominated_indices = nds.do(feasible_obj[objectives].values,
                                  only_non_dominated_front=True)

    non_dominated_obj = feasible_obj.iloc[nondominated_indices, :].copy()
    non_dominated_dvar = feasible_dvar.iloc[nondominated_indices, :].copy()

    non_dominated_dvar.drop_duplicates(inplace=True)
    non_dominated_obj = non_dominated_obj.loc[non_dominated_dvar.index]

    # Save the result.
    non_dominated_obj.to_hdf(out_path, key='obj')
    non_dominated_dvar.to_hdf(out_path, key='dvar')
