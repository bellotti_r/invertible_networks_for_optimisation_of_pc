import os
import json
import copy
from itertools import product
import numpy as np
import pandas as pd


if __name__ == '__main__':
    scan_name = 'forward_model'
    #scan_name = 'test'
    save_dir = f'/data/user/bellotti_r/hyperparameter_scans/{scan_name}'
    cores_per_job = 12

    if not os.path.exists(save_dir):
        os.makedirs(save_dir)

    # create the log directory
    log_dir = f'{save_dir}/logs'
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)

    # path to the SLURM batch template
    # It contains 3 placeholders:
    # One for number of CPU cores to use for each parameter configuration.
    # One for the  job name.
    # One for the path to the experiment config file to run.
    slurm_batch_script_template_file = '/psi/home/bellotti_r/bellotti_r/run_parameter_config.slurm'

    fix_parameters = {
        'experiment_name': 'forward_scan',
        'training_dataset_name': '4_peak_distr_lower_charge 20k slinear 2D_filtered',
        'training_data_paths': ['/data/user/bellotti_r/datasets/4_peak_distr_lower_charge 20k slinear 2D_filtered.hdf5'],
        'test_dataset_name': '4_peak_distr_lower_charge_and_IM slinear 2D_filtered corrected 1k slinear 2D_filtered',
        'test_data_paths': ['/data/user/bellotti_r/datasets/4_peak_distr_lower_charge_and_IM slinear 2D_filtered corrected 1k slinear 2D_filtered.hdf5'],
        'predicted_qois': [
            'Mean Bunch Energy',
            'RMS Beamsize in x',
            'RMS Beamsize in y',
            'Normalized Emittance x',
            'Normalized Emittance y',
            'energy spread of the beam',
            'Correlation xpx',
            'Correlation ypy'
        ],
        'dvar_bounds': {
            'IBF': [450.0, 550.0],
            'IM': [100.0, 260.0],
            'GPHASE': [-50.0, 10.0],
            'ILS1': [0.0,  250.0],
            'ILS2': [0.0, 200.0],
            'ILS3': [0.0, 200.0],
            'bunchCharge': [0.3, 5.0],
            'lambda': [0.3, 2.0],
            'SIGXY': [1.5, 12.5],
            'Path length': [0.0, 26.0]
        },
        'qoi_bounds': {
            'Mean Bunch Energy': [0.0, 70.0],
            'RMS Beamsize in x': [0.0, 2.05],
            'RMS Beamsize in y': [0.0, 2.05],
            'Normalized Emittance x': [0.0, 0.018],
            'Normalized Emittance y': [0.0, 0.018],
            'energy spread of the beam': [0.0, 32.0],
            'Correlation xpx': [-1.0, 1.0],
            'Correlation ypy': [-1.0, 1.0]
        },
        'model_type': 'dense',
        'model_parameters': {
            'hiddenLayers': None,
            'unitsPerLayer': None,
            'activation': 'relu',
            'batch_size': None,
            'learning_rate': 0.0001,
            'optimizer': 'adam',
            # quantity to monitor,
            # patience (=epochs to continue without improvement
            # in the monitored quantity,
            # minimum decrease that is considered an improvement,
            # number of epochs
            'epochs': 'val_mean_absolute_percentage_error,20,1.,200'
        },
        'save_dir': save_dir
    }

    parameters_to_scan = {
        'hiddenLayers': [6, 7, 8, 9, 10],
        'unitsPerLayer': [300, 400, 500, 600, 700],
        'batch_size': [128, 256],
        'learning_rate': [0.0001]
    }
    
    #parameters_to_scan = {
    #    'hidden_layers': [1],
    #    'unitsPerLayer': [1],
    #    'batch_size': [1],
    #    'learning_rate': [0.0001]
    #}

    parameters = []

    config_dir = f'{save_dir}/configurations'
    if not os.path.exists(config_dir):
        os.makedirs(config_dir)

    # write the batch script
    batch_dir = f'{save_dir}/batch_scripts'
    if not os.path.exists(batch_dir):
        os.makedirs(batch_dir)

    # load the template
    with open(slurm_batch_script_template_file, 'r') as file:
        template = file.read()

    # create the parameter configs,  and the batch scripts
    for param_ID, parameter_values in enumerate(product(*parameters_to_scan.values())):
        parameters.append(parameter_values)

        # create the experiment config file
        config = copy.deepcopy(fix_parameters)
        config['experiment_name'] = f'{scan_name}_{param_ID}'
        for i, key in enumerate(parameters_to_scan.keys()):
            config['model_parameters'][key] = parameter_values[i]

        # write the experiment config file
        json_path = f'{config_dir}/{param_ID}.json'
        with open(json_path, 'w') as file:
            json.dump(config, file, indent='\t')

        # create and write the batch script
        job_name = f'{scan_name}_{param_ID}'
        batch_script = template.format(cores_per_job, job_name, json_path)
        with open(f'{batch_dir}/{param_ID}.slurm', 'w') as file:
            file.write(batch_script)

    # log all the parameter values to a CSV file
    parameters = np.vstack(parameters)
    parameters = pd.DataFrame(data=parameters, columns=parameters_to_scan.keys())
    parameters.to_csv(f'{config_dir}/parameters.csv')
    print(parameters)
