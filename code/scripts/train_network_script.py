from bellotti_r_master_thesis.experiment_config import ExperimentConfig
from bellotti_r_master_thesis.awa_training import train_awa_forward_model,\
                                                  train_awa_invertible_model
from mllib.model import KerasSurrogate
import os
import logging
import argparse
import numpy as np
import pandas as pd
import tensorflow as tf
tf.keras.backend.set_floatx('float64')


def absolute_percentage_error(y_true, y_pred, qoi_columns):
    error = (y_true - y_pred) / y_true * 100.
    error = pd.DataFrame(data=np.abs(error), columns=qoi_columns)
    return error


if __name__ == '__main__':
    # reproducibility
    SEED = 9357
    tf.random.set_seed(SEED)
    np.random.seed(SEED)

    # use 30% of the OPAL runs for validation
    validation_fraction = 0.3
    training_fraction = 1. - validation_fraction

    # parse the arguments
    parser = argparse.ArgumentParser(description='Train a neural network.')
    parser.add_argument('config_file', help='Experiment configuration to run.')

    args = parser.parse_args()

    if not args.config_file:
        exit()

    # load experiment config
    config = ExperimentConfig(args.config_file)

    # log info
    version = f'tensorflow version: {tf.__version__}'

    logging.basicConfig(filename=f'{config.save_dir}/logs/{config.experiment_name}.log',
                        level=logging.INFO)
    logging.info(version)

    logging.info('Name: ' + config.experiment_name)

    # path to save the trained model at
    model_dir = f'{config.save_dir}/models'

    # load the data
    X_train, y_train, X_val, y_val = config.train_val_split(training_fraction)

    # round the path length to make sure rounding errors don't affect the
    # groupby() in the evaluation
    X_train['Path length'] = np.round(X_train['Path length'], decimals=3)
    X_val['Path length'] = np.round(X_val['Path length'], decimals=3)

    qoi_cols = y_train.columns

    quantiles_to_log = [0., 0.25, 0.5, 0.75, 0.9, 0.95, 0.99, 1.]

    # path to log the losses and metrics to
    tensorboard_dir = f'{config.save_dir}/tensorboard_{config.experiment_name}'
    config.model_parameters['tensorboard_dir'] = tensorboard_dir

    batch_size = config.model_parameters['batch_size']

    if config.model_type == 'dense':
        surr = train_awa_forward_model(config.dvar_bounds,
                                       X_train,
                                       y_train,
                                       X_val,
                                       y_val,
                                       config.model_parameters,
                                       tensorboard_dir,
                                       version,
                                       config.experiment_name)

        # log the performance
        y_train_predict = surr.predict(X_train.values,
                                       batch_size=batch_size)
        y_val_predict = surr.predict(X_val.values,
                                     batch_size=batch_size)

        # relative error [%]
        rel_error_train = absolute_percentage_error(y_train,
                                                    y_train_predict,
                                                    qoi_cols)
        rel_error_val = absolute_percentage_error(y_val,
                                                  y_val_predict,
                                                  qoi_cols)

        rel_error_train['Path length'] = X_train['Path length']
        rel_error_val['Path length'] = X_val['Path length']

        performance_dir = f'{config.save_dir}/performance/{config.experiment_name}'
        if not os.path.exists(performance_dir):
            os.makedirs(performance_dir)

        # descriptive statistics for each QOI
        rel_error_train_path = f'{performance_dir}/rel_error_train.csv'
        rel_error_val_path = f'{performance_dir}/rel_error_val.csv'

        to_log_train = rel_error_train.groupby('Path length')
        to_log_train = to_log_train.quantile(quantiles_to_log)
        to_log_val = rel_error_val.groupby('Path length')
        to_log_val = to_log_val.quantile(quantiles_to_log)

        to_log_train.to_csv(rel_error_train_path)
        to_log_val.to_csv(rel_error_val_path)

        # descriptive statistics for the maximum error of the prediction
        max_rel_error_train = rel_error_train.drop(columns='Path length')
        max_rel_error_train = max_rel_error_train.max(axis=1)
        max_rel_error_train = pd.DataFrame({
            'maximum percentage error': max_rel_error_train,
            'Path length': X_train['Path length'].copy(),
        })
        max_rel_error_val = rel_error_val.drop(columns='Path length')
        max_rel_error_val = max_rel_error_val.max(axis=1)
        max_rel_error_val = pd.DataFrame({
            'maximum percentage error': max_rel_error_val,
            'Path length': X_val['Path length'].copy(),
        })

        max_rel_error_train.to_csv(f'{performance_dir}/max_rel_error_train.csv')
        max_rel_error_val.to_csv(f'{performance_dir}/max_rel_error_val.csv')

    elif config.model_type == 'invertible':
        surr = train_awa_invertible_model(config.qoi_bounds,
                                          X_train,
                                          y_train,
                                          X_val,
                                          y_val,
                                          config.model_parameters,
                                          version,
                                          config.experiment_name)
        params = config.model_parameters

        # load the validation model (needed to asses sampling performance)
        val_surr = KerasSurrogate.load(*params['validation_model'])

        ######################################################
        # log the performance for the forward prediction
        ######################################################
        # predict
        y_train_predict = surr.predict(X_train.values,
                                       batch_size=batch_size)
        y_val_predict = surr.predict(X_val.values,
                                     batch_size=batch_size)

        y_train['Path length'] = X_train['Path length'].copy()
        y_val['Path length'] = X_val['Path length'].copy()

        y_train_predict = pd.DataFrame(data=y_train_predict,
                                       columns=y_train.columns)
        y_val_predict = pd.DataFrame(data=y_val_predict,
                                     columns=y_val.columns)

        # calculate the performance metrics
        rel_error_train = np.abs((y_train_predict - y_train) / y_train)
        rel_error_train = pd.DataFrame(data=rel_error_train,
                                       columns=y_train.columns)
        rel_error_val = np.abs((y_val_predict - y_val) / y_val)
        rel_error_val = pd.DataFrame(data=rel_error_val,
                                     columns=y_val.columns)

        rel_error_train['Path length'] = X_train['Path length'].copy()
        rel_error_val['Path length'] = X_val['Path length'].copy()

        # save the performance metrics to disk
        performance_dir = f'{config.save_dir}/performance/{config.experiment_name}'
        if not os.path.exists(performance_dir):
            os.makedirs(performance_dir)

        rel_error_train_path = f'{performance_dir}/rel_error_train.csv'
        rel_error_val_path = f'{performance_dir}/rel_error_val.csv'

        to_log_train = rel_error_train.groupby('Path length')
        to_log_train = to_log_train.quantile(quantiles_to_log)
        to_log_val = rel_error_val.groupby('Path length')
        to_log_val = to_log_val.quantile(quantiles_to_log)

        to_log_train.to_csv(rel_error_train_path)
        to_log_val.to_csv(rel_error_val_path)

        # descriptive statistics for the maximum error of the prediction
        max_rel_error_train = rel_error_train.drop(columns='Path length')
        max_rel_error_train = max_rel_error_train.max(axis=1)
        max_rel_error_train = pd.DataFrame({
            'maximum percentage error': max_rel_error_train,
            'Path length': X_train['Path length'].copy(),
        })
        max_rel_error_val = rel_error_val.drop(columns='Path length')
        max_rel_error_val = max_rel_error_val.max(axis=1)
        max_rel_error_val = pd.DataFrame({
            'maximum percentage error': max_rel_error_val,
            'Path length': X_val['Path length'].copy(),
        })

        max_rel_error_train.to_csv(f'{performance_dir}/max_rel_error_train.csv')
        max_rel_error_val.to_csv(f'{performance_dir}/max_rel_error_val.csv')

        ####################################################
        # log the performance for the inverse prediction
        ####################################################
        # sample and average sampled configs
        sampled_dvars_train = []
        sampled_dvars_val = []
        for _ in range(params['num_dvars_to_average_for_validation']):
            sample_train = surr.sample(y_train.values, batch_size=batch_size)
            sampled_dvars_train.append(sample_train)

            sample_val = surr.sample(y_val.values, batch_size=batch_size)
            sampled_dvars_val.append(sample_val)

        sampled_dvars_train = np.stack(sampled_dvars_train, axis=0)
        sampled_dvars_train = np.mean(sampled_dvars_train, axis=0)

        sampled_dvars_val = np.stack(sampled_dvars_val, axis=0)
        sampled_dvars_val = np.mean(sampled_dvars_val, axis=0)

        sampled_dvars_train = pd.DataFrame(data=sampled_dvars_train,
                                           columns=X_train.columns)
        sampled_dvars_val = pd.DataFrame(data=sampled_dvars_val,
                                           columns=X_val.columns)

        # add the path length
        sampled_dvars_train['Path length'] = X_train['Path length']
        sampled_dvars_val['Path length'] = X_val['Path length']

        # reorder the columns
        sampled_dvars_train = sampled_dvars_train[['IBF',
                                                   'IM',
                                                   'GPHASE',
                                                   'ILS1',
                                                   'ILS2',
                                                   'ILS3',
                                                   'bunchCharge',
                                                   'lambda',
                                                   'SIGXY',
                                                   'Path length']]
        sampled_dvars_val = sampled_dvars_val[['IBF',
                                               'IM',
                                               'GPHASE',
                                               'ILS1',
                                               'ILS2',
                                               'ILS3',
                                               'bunchCharge',
                                               'lambda',
                                               'SIGXY',
                                               'Path length']]

        # use the validation model to predict the quality of the sampled DVARs
        y_train_predict = val_surr.predict(sampled_dvars_train.values,
                                           batch_size=batch_size)
        y_val_predict = val_surr.predict(sampled_dvars_val.values,
                                         batch_size=batch_size)

        y_train_predict = pd.DataFrame(data=y_train_predict,
                                       columns=['Mean Bunch Energy',
                                                'RMS Beamsize in x',
                                                'RMS Beamsize in y',
                                                'Normalized Emittance x',
                                                'Normalized Emittance y',
                                                'energy spread of the beam',
                                                'Correlation xpx',
                                                'Correlation ypy'])
        y_val_predict = pd.DataFrame(data=y_val_predict,
                                     columns=['Mean Bunch Energy',
                                              'RMS Beamsize in x',
                                              'RMS Beamsize in y',
                                              'Normalized Emittance x',
                                              'Normalized Emittance y',
                                              'energy spread of the beam',
                                              'Correlation xpx',
                                              'Correlation ypy'])

        # bring predicted values into the shape the INN was fitted against
        y_train_predict.drop(columns=[
                                'Correlation xpx',
                                'Correlation ypy'
                             ], inplace=True)
        y_val_predict.drop(columns=[
                                'Correlation xpx',
                                'Correlation ypy'
                            ], inplace=True)

        # remove the unnecessary columns again
        y_train.drop(columns='Path length', inplace=True)
        y_val.drop(columns='Path length', inplace=True)

        # calculate the performance metrics
        rel_error_train = ((y_train_predict - y_train) / y_train).abs() * 100.
        rel_error_val = ((y_val_predict - y_val) / y_val).abs() * 100.

        # write the performance to disk
        performance_dir = f'{config.save_dir}/performance/{config.experiment_name}'
        if not os.path.exists(performance_dir):
            os.makedirs(performance_dir)

        rel_error_train_path = f'{performance_dir}/sampling_rel_error_train.csv'
        rel_error_val_path = f'{performance_dir}/sampling_rel_error_val.csv'

        rel_error_train.describe().to_csv(rel_error_train_path)
        rel_error_val.describe().to_csv(rel_error_val_path)

        # descriptive statistics for the maximum error of the prediction
        max_rel_error_train = rel_error_train.copy()
        max_rel_error_train = max_rel_error_train.max(axis=1)
        max_rel_error_train = pd.DataFrame({
            'maximum percentage error': max_rel_error_train,
            'Path length': X_train['Path length'].copy(),
        })
        max_rel_error_val = rel_error_val.copy()
        max_rel_error_val = max_rel_error_val.max(axis=1)
        max_rel_error_val = pd.DataFrame({
            'maximum percentage error': max_rel_error_val,
            'Path length': X_val['Path length'].copy(),
        })

        max_rel_error_train.to_csv(f'{performance_dir}/sampling_max_rel_error_train.csv')
        max_rel_error_val.to_csv(f'{performance_dir}/sampling_max_rel_error_val.csv')
    else:
        raise NotImplementedError

    # save the surrogate model
    surr.save(directory=model_dir)

    # save the model name
    name_dir = f'{config.save_dir}/model_names'
    name_file = f'{name_dir}/{config.experiment_name}.txt'
    if not os.path.exists(name_dir):
        os.makedirs(name_dir)
    with open(name_file, 'w') as file:
        file.write(config.experiment_name)
