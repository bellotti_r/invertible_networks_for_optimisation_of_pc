import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
import seaborn as sns

from bellotti_r_master_thesis.plot_optimisation_results import draw_confidence_intervals


to_plot = [
    'RMS Beamsize in x',
    'Normalized Emittance x',
    'energy spread of the beam',
    'Correlation xpx'
]

objective_labels = {
    'RMS Beamsize in x': r'$\sigma_x$ [mm]',
    'Normalized Emittance x': r'$\epsilon_x$ [mm mrad]',
    'energy spread of the beam': r'$\Delta E$ [keV]',
    'Correlation xpx': r'$\mathrm{Corr}(x, p_x)$',
}

blue = (0.12156862745098039, 0.4666666666666667, 0.7058823529411765)
orange = (1.0, 0.4980392156862745, 0.054901960784313725)

cmap_levels = [
    ListedColormap([
        (0, 0, 0, 0),
        (*blue, 0.2),
        (*blue, 0.4),
        (*blue, 0.6)
    ]),
    ListedColormap([
        (0, 0, 0, 0),
        (*orange, 0.2),
        (*orange, 0.4),
        (*orange, 0.6)
    ])
]


def plot_cv_violation(cv, ax, label):
    minimum = np.min(cv, axis=1)
    maximum = np.max(cv, axis=1)
    avg_fw = np.mean(cv, axis=1)

    ax.fill_between(x=np.arange(minimum.shape[0]),
                    y1=minimum,
                    y2=maximum,
                    alpha=0.3)
    ax.plot(np.arange(minimum.shape[0]), avg_fw, label=label)

    ax.set_ylim([1e-9, 2.5e2])
    ax.set_yscale('log')
    ax.grid(True)

    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)

    ax.set_xlabel('Generation', fontsize=26)
    ax.set_ylabel('CV', fontsize=26)
    ax.tick_params(labelsize=20)


def plot_objectives_convergence(df, axes, limits={
    'RMS Beamsize in x': [0., 15.],
    'Normalized Emittance x': [1., 100.],
    'energy spread of the beam': [10., 1000.],
    'Correlation xpx': [-0.2, 0.2],
}):
    minimum = df.groupby('generation').min()
    maximum = df.groupby('generation').max()
    avg = df.groupby('generation').mean()

    for col, ax in zip(to_plot, axes.flatten()):
        ax.fill_between(x=minimum.index,
                        y1=minimum[col],
                        y2=maximum[col],
                        alpha=0.3)
        ax.plot(avg.index, avg[col], label='forward')
        ax.set_ylabel(objective_labels[col], fontsize=26)
        ax.set_ylim(limits[col])
        if col in ['Normalized Emittance x', 'energy spread of the beam']:
            ax.set_yscale('log')

    # style the plots
    for ax in axes.flatten():
        ax.grid(True)

        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)

        ax.tick_params(labelsize=20)
    # Put xlabel only on bottom plots.
    for ax in axes[1, :]:
        ax.set_xlabel('Generation', fontsize=26)


def plot_nondominated_set(objectives,
                          colormap,
                          axes,
                          quantiles_to_plot=[0.9],
                          opal_validation=None,
                          objective_limits={
                              'RMS Beamsize in x': [0., 3.],
                              'Normalized Emittance x': [0., 15.],
                              'energy spread of the beam': [0., 250.],
                              'Correlation xpx': [-0.2, 0.2],
                          },
                          color_for_dots=None,
                          remove_upper_corner=False,
                          training_qoi=None):
    quantiles_to_plot = [0.9]

    # plot
    for i in range(len(to_plot)):
        for j in range(len(to_plot)):
            ax = axes[i, j]

            if (j > i):
                if remove_upper_corner:
                    ax.remove()
                continue

            obj = objectives
            if opal_validation is not None:
                obj = opal_validation
            if i == j:
                sns.distplot(obj[to_plot[i]],
                             color=color_for_dots,
                             ax=ax)
            else:
                sns.scatterplot(data=obj,
                                x=to_plot[j],
                                y=to_plot[i],
                                color=color_for_dots,
                                zorder=2,
                                legend=False,
                                ax=ax)

                col_x = to_plot[j]
                col_y = to_plot[i]

                residual_quantiles_x = [residual_quantiles.query('`quantile` == @q')[col_x].values for q in quantiles_to_plot]
                residual_quantiles_y = [residual_quantiles.query('`quantile` == @q')[col_y].values for q in quantiles_to_plot]

                draw_confidence_intervals(objectives[col_x].values,
                                          objectives[col_y].values,
                                          quantiles_to_plot,
                                          residual_quantiles_x,
                                          residual_quantiles_y,
                                          objective_limits[col_x],
                                          objective_limits[col_y],
                                          50,
                                          colormap,
                                          ax)

                # plot the training data (if needed)
                if training_qoi is not None:
                    ax.plot(training_qoi[col_x],
                            training_qoi[col_y],
                            marker='o',
                            linestyle='',
                            markersize=0.5,
                            color=sns.color_palette()[2])

                ax.set_xlim(objective_limits[col_x])
                ax.set_ylim(objective_limits[col_y])

            # remove labels that are generated by seaborn
            ax.set_xlabel('')
            ax.set_ylabel('')

            if i == len(to_plot)-1:
                ax.set_xlabel(objective_labels[to_plot[j]], fontsize=25)
            if j == 0:
                ax.set_ylabel(objective_labels[to_plot[i]], fontsize=25)

    for ax in axes.flatten():
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.tick_params(axis='both', labelsize=18)

    # plot constraint region for the correlations
    for ax in axes[-1, :-1]:
        ax.fill_between(x=ax.get_xlim(),
                        y1=[-0.1, -0.1],
                        y2=[0.1, 0.1],
                        color='gray',
                        zorder=0,
                        alpha=0.3)


if __name__ == '__main__':
    ###############
    # Config
    ###############
    problem = 'awa'
    base_dir = '/data/user/bellotti_r/paper_new'

    # directory where the results of the optimisation are stored
    save_dir = f'{base_dir}/optimisation/{problem}'
    # load prediction uncertainty
    path_to_residual_quantiles = f'{base_dir}/invertible_network_paper/plots/models/awa_forward_model/forward_prediction_residual_quantiles_by_longitudinal_pos.csv'

    plot_dir = f'{base_dir}/invertible_network_paper/plots/optimisation/{problem}'
    if not os.path.exists(plot_dir):
        os.makedirs(plot_dir)
    dpi = 300

    plot_validated_points = False

    ###############
    # Load
    ###############
    print('Loading data...')
    # load optimal objectives
    file = f'{save_dir}/forward_model/optimal_qois.hdf5'
    optimal_objectives_forward_surr = pd.read_hdf(file)

    file = f'{save_dir}/invertible_model/optimal_qois.hdf5'
    optimal_objectives_INN_and_forward_surr = pd.read_hdf(file)

    # OPAL validation of the non-dominated configurations
    if plot_validated_points:
        optimal_obj_opal_FW = pd.read_hdf(f'{save_dir}/forward_model_validation/objectives.hdf5')
        optimal_obj_opal_INN_and_FW = pd.read_hdf(f'{save_dir}/invertible_model_validation/objectives.hdf5')
        # Use reasonable units.
        # The other quantities are already saved like this,
        # but OPAL uses SI units.
        for df in [optimal_obj_opal_FW, optimal_obj_opal_INN_and_FW]:
            df['RMS Beamsize in x'] *= 1000.
            df['Normalized Emittance x'] *= 1e6
            df['energy spread of the beam'] *= 1000.
    else:
        optimal_obj_opal_FW = None
        optimal_obj_opal_INN_and_FW = None

    residual_quantiles = pd.read_csv(path_to_residual_quantiles,
                                     index_col=(0, 1))
    residual_quantiles = residual_quantiles.reset_index()
    residual_quantiles = residual_quantiles.rename(columns={'level_1': 'quantile'})
    residual_quantiles['Path length'] = np.round(residual_quantiles['Path length'], decimals=2)
    residual_quantiles = residual_quantiles.query('`Path length` >= 25.95')

    # Convergence: Constraint violations (CV)
    file = f'{save_dir}/forward_model/constraint_violations.npy'
    cv_forward_surr = np.load(file)

    file = f'{save_dir}/invertible_model/constraint_violations.npy'
    cv_INN_and_forward_surr = np.load(file)

    # Convergence: Objectives (by generation)
    file = f'{save_dir}/forward_model/objective_generations.hdf5'
    objective_generations_fw = pd.read_hdf(file)

    file = f'{save_dir}/invertible_model/objective_generations.hdf5'
    objective_generations_INN_fw = pd.read_hdf(file)

    qoi_trainval = pd.read_hdf('/data/user/bellotti_r/paper_new/datasets/awa_training_26m.hdf5', key='qoi')
    qoi_trainval['RMS Beamsize in x'] *= 1000.
    qoi_trainval['Normalized Emittance x'] *= 1e6
    qoi_trainval['energy spread of the beam'] *= 1000.

    ##################
    # Plot
    ##################
    print('Plotting convergence...')
    # Convergence: Constraint violation
    fig, ax = plt.subplots(figsize=(16, 4.5))
    plot_cv_violation(cv_forward_surr, ax, 'Random init')
    plot_cv_violation(cv_INN_and_forward_surr, ax, 'Invertible Net')
    fig.legend(fontsize=20)
    fig.tight_layout()

    fig.savefig(f'{plot_dir}/convergence_cv.jpg', dpi=2*dpi)

    # Convergence: Objectives
    fig, axes = plt.subplots(2, 2, figsize=(16, 6), sharex='col')

    # plot curves for the optimisation with the FW model only
    # and with the INN initialisation and the FW model
    plot_objectives_convergence(objective_generations_fw, axes)
    plot_objectives_convergence(objective_generations_INN_fw, axes)
    fig.align_labels()
    fig.tight_layout()

    fig.savefig(f'{plot_dir}/convergence_objectives.jpg', dpi=dpi)

    # non-dominated set
    print('Plotting non-dominated set...')
    fig, axes = plt.subplots(len(to_plot), len(to_plot), figsize=(16, 16))

    plot_nondominated_set(optimal_objectives_forward_surr,
                          cmap_levels[0],
                          axes,
                          opal_validation=optimal_obj_opal_FW)
    plot_nondominated_set(optimal_objectives_INN_and_forward_surr,
                          cmap_levels[1],
                          axes,
                          opal_validation=optimal_obj_opal_INN_and_FW,
                          # remove the upper corner of the subplot matrix
                          remove_upper_corner=True)
    # add the color coding labels
    fig.text(x=0.6,
             y=0.9,
             s='Random init',
             fontsize=50,
             fontweight='bold',
             color=blue)
    fig.text(x=0.6,
             y=0.85,
             s='Invertible Net',
             fontsize=50,
             fontweight='bold',
             color=orange)

    fig.tight_layout()
    fig.savefig(f'{plot_dir}/nondominated_set.jpg', dpi=dpi)

    # non-dominated set with training dots
    print('Plotting non-dominated set...')
    fig, axes = plt.subplots(len(to_plot), len(to_plot), figsize=(16, 16))

    plot_nondominated_set(optimal_objectives_forward_surr,
                          cmap_levels[0],
                          axes,
                          opal_validation=optimal_obj_opal_FW)
    plot_nondominated_set(optimal_objectives_INN_and_forward_surr,
                          cmap_levels[1],
                          axes,
                          opal_validation=optimal_obj_opal_INN_and_FW,
                          # remove the upper corner of the subplot matrix
                          remove_upper_corner=True,
                          training_qoi=qoi_trainval)
    # add the color coding labels
    fig.text(x=0.6,
             y=0.9,
             s='Random init',
             fontsize=50,
             fontweight='bold',
             color=blue)
    fig.text(x=0.6,
             y=0.85,
             s='Invertible Net',
             fontsize=50,
             fontweight='bold',
             color=orange)

    fig.tight_layout()
    fig.savefig(f'{plot_dir}/nondominated_set_with_training.jpg', dpi=dpi)

    # fig, axes = plt.subplots(len(to_plot), len(to_plot), figsize=(16, 16))
    # plot_nondominated_set(optimal_objectives_INN_and_forward_surr,
    #                       cmap_levels[1],
    #                       axes,
    #                       opal_validation=optimal_obj_opal_INN_and_FW,
    #                       # remove the upper corner of the subplot matrix
    #                       remove_upper_corner=True,
    #                       objective_limits={
    #                         'RMS Beamsize in x': [0., 1.2],
    #                         'Normalized Emittance x': [0., 10.],
    #                         'energy spread of the beam': [0., 115.],
    #                         'Correlation xpx': [-1., 1.],
    #                       },
    #                       color_for_dots=orange)
    #
    # fig.tight_layout()
    # fig.savefig(f'{plot_dir}/nondominated_set_INN_only.jpg', dpi=dpi)
