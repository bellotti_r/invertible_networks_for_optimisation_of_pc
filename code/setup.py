import setuptools

setuptools.setup(
    name='turning_tables',
    version='0.1',
    author='Renato Bellotti',
    description='Code for paper "Turning the Tables: Invertible Models for Particle Accelerators"',
    url='https://gitlab.psi.ch/bellotti_r/invertible_network_paper',
    packages=setuptools.find_packages(),
    python_requires='>=3.7',
)
