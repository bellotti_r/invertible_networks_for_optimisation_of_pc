import logging
from mllib.model import FixedMinMaxScaler,\
                        AdaptiveMinMaxScaler,\
                        LogarithmTransform,\
                        QuantileTransformerPreprocessor,\
                        PreprocessorPipeline,\
                        ShiftPositive,\
                        KerasSurrogate
from bellotti_r_master_thesis.training import build_model,\
                                              train_entire_set_with_validation
from bellotti_r_master_thesis.invertible_network.invertible_neural_network import InvertibleNetworkSurrogate
import tensorflow as tf
tf.keras.backend.set_floatx('float64')


def train_awa_forward_model(dvar_bounds,
                            X_train,
                            y_train,
                            X_val,
                            y_val,
                            model_parameters,
                            tensorboard_dir,
                            version,
                            name='awa_forward_model'):
    '''
    Creates and trains a forward model for the AWA.

    Parameters
    ==========
    dvar_bounds: dict
        Contains the intervals (2-tuples) for each design variable.
        The keys are the names of the DVARs as strings.
    X_train, y_train: pandas.DataFrame
        Training data.
    X_val, y_val: pandas.DataFrame
        Validation data.
    model_parameters: dict
        Contains the hyperparameters for the tf.Keras model.
        Must contain the following keys:
        - hiddenLayers
        - unitsPerLayer
        - activation
        - optimizer
        - learning_rate
        - batch_size
        - epochs
    tensorboard_dir: str
        Path where TensorBoard will log the training progress.
    version: str
        Describes the versions of the used libraries.
    name: str
        Name of the surrogate model.
    '''
    # scale the data
    x_scaler = FixedMinMaxScaler(bounds=dvar_bounds,
                                 columns=X_train.columns,
                                 a=-1., b=1.)

    y_scaler = PreprocessorPipeline([
        ShiftPositive(offset=100.),
        LogarithmTransform(),
        AdaptiveMinMaxScaler(a=-1., b=1.)
    ])

    x_scaler.fit(X_train.values)
    y_scaler.fit(y_train.values)

    X_train = x_scaler.transform(X_train.values)
    y_train = y_scaler.transform(y_train.values)

    X_val = x_scaler.transform(X_val.values)
    y_val = y_scaler.transform(y_val.values)

    # build the model
    number_of_inputs = X_train.shape[1]
    number_of_outputs = y_train.shape[1]
    model = build_model(model_parameters,
                        number_of_inputs, number_of_outputs)
    logging.info(model.summary())

    # short names for some variables
    batch_size = model_parameters['batch_size']

    # train the model
    model = train_entire_set_with_validation(model,
                                             X_train, y_train,
                                             X_val, y_val,
                                             batch_size,
                                             model_parameters['epochs'],
                                             tensorboard_dir)

    # build a surrogate model from the preprocessors and the Keras model
    return KerasSurrogate(model, x_scaler, y_scaler, name, version)


def train_awa_invertible_model(qoi_bounds,
                               X_train,
                               y_train,
                               X_val,
                               y_val,
                               params,
                               version,
                               name):
    '''
    Creates and trains an invertible model for the AWA.

    Parameters
    ==========
    qoi_bounds: dict
        Contains the intervals (2-tuples) for each quantity of interest.
        The keys are the names of the QOIs as strings.
    X_train, y_train: pandas.DataFrame
        Training data.
    X_val, y_val: pandas.DataFrame
        Validation data.
    model_parameters: dict
        Contains the hyperparameters for the tf.Keras model.
        Must contain the following keys:
        - validation_model
        - num_dvars_to_average_for_validation
        - x_dim
        - y_dim
        - z_dim
        - nominal_dim
        - coefficient_net_activations
        - coefficient_net_units
        - sampling_distribution
        - optimizer
        - learning_rate
        - epochs
        - batch_size
        - number_of_blocks
        - loss_weight_artificial
        - loss_weight_reconstruction
        - loss_weight_x
        - loss_weight_y
        - loss_weight_z
        - save_dir
        - tensorboard_dir
    version: str
        Describes the versions of the used libraries.
    name: str
        Name of the surrogate model.
    '''
    # Copy input because we need to modify them.
    X_train = X_train.copy()
    y_train = y_train.copy()
    X_val = X_val.copy()
    y_val = y_val.copy()
    params = params.copy()

    # Define the preprocessors.
    x_scaler = PreprocessorPipeline([
        QuantileTransformerPreprocessor(output_distribution='normal'),
        AdaptiveMinMaxScaler(a=-1., b=1.)
    ])
    y_scaler = FixedMinMaxScaler(bounds=qoi_bounds,
                                 columns=y_train.columns,
                                 a=-1., b=1.)

    # Build the surrogate model.
    surr = InvertibleNetworkSurrogate.from_config(
        params['x_dim'],
        params['y_dim'],
        params['z_dim'],
        params['nominal_dim'],
        params['number_of_blocks'],
        params['coefficient_net_units'],
        params['coefficient_net_activations'],
        True,
        x_scaler,
        y_scaler,
        name,
        params['sampling_distribution'],
        version)
    # Log the number of parameters.
    surr.model.summary()

    # Build the optimizer.
    if params['optimizer'] == 'adam':
        lr = params['learning_rate']
        params['optimizer'] = tf.keras.optimizers.Adam(learning_rate=lr)
    else:
        raise NotImplementedError

    # Copy the longitudinal position to the QOIs to allow sampling for
    # QOIs given at a user defined position.
    cols = list(y_train.columns)
    cols.append('Path length')
    assert (y_train.index == X_train.index).all()
    assert (y_val.index == X_val.index).all()
    y_train['Path length'] = X_train['Path length']
    y_val['Path length'] = X_val['Path length']
    # needed to make sure that "Path length" is the last column
    y_train = y_train[cols]
    y_val = y_val[cols]

    y_train.reset_index(drop=True, inplace=True)
    y_val.reset_index(drop=True, inplace=True)
    X_train.reset_index(drop=True, inplace=True)
    X_val.reset_index(drop=True, inplace=True)
    logging.info(f'Design variables: {X_train.columns}')
    logging.info(f'Quantities of interest: {y_train.columns}')

    # Train.
    surr.fit(X_train.values, y_train.values,
             X_val.values, y_val.values,
             **params)

    return surr
