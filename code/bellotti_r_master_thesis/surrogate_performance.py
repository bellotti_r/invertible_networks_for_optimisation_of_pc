import datetime
import math
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

class ForwardModelPerformance:
    '''Calculates and plots performance metrics for an forward model.'''

    def init_predictions_test(self):
        self._y_diff = self._Y_true - self._Y_predict
        self._y_diff_rel = self._y_diff / self._Y_true
        self._y_diff_rel_percentage = 100. * self._y_diff_rel
        
        # [sigma] = mm
        self._y_diff['RMS Beamsize in x'] *= 1000.
        self._y_diff['RMS Beamsize in y'] *= 1000.
        self._Y_true['RMS Beamsize in x'] *= 1000.
        self._Y_true['RMS Beamsize in y'] *= 1000.
        self._Y_predict['RMS Beamsize in x'] *= 1000.
        self._Y_predict['RMS Beamsize in y'] *= 1000.

        # [epsilon] = mm rad
        self._y_diff['Normalized Emittance x'] *= 1000.*1000.
        self._y_diff['Normalized Emittance y'] *= 1000.*1000.
        self._Y_true['Normalized Emittance x'] *= 1000.*1000.
        self._Y_true['Normalized Emittance y'] *= 1000.*1000.
        self._Y_predict['Normalized Emittance x'] *= 1000.*1000.
        self._Y_predict['Normalized Emittance y'] *= 1000.*1000.

        # [dE] = keV
        self._y_diff['energy spread of the beam'] *= 1000.
        self._Y_true['energy spread of the beam'] *= 1000.
        self._Y_predict['energy spread of the beam'] *= 1000.

    def init_predictions_train(self):
        self._y_diff_train = self._Y_true_train - self._Y_predict_train
        self._y_diff_rel_train = self._y_diff_train / self._Y_true_train
        self._y_diff_rel_percentage_train = 100. * self._y_diff_rel_train
        
        # [sigma] = mm
        self._y_diff_train['RMS Beamsize in x'] *= 1000.
        self._y_diff_train['RMS Beamsize in y'] *= 1000.
        self._Y_true_train['RMS Beamsize in x'] *= 1000.
        self._Y_true_train['RMS Beamsize in y'] *= 1000.
        self._Y_predict_train['RMS Beamsize in x'] *= 1000.
        self._Y_predict_train['RMS Beamsize in y'] *= 1000.

        # [epsilon] = mm rad
        self._y_diff_train['Normalized Emittance x'] *= 1000.*1000.
        self._y_diff_train['Normalized Emittance y'] *= 1000.*1000.
        self._Y_true_train['Normalized Emittance x'] *= 1000.*1000.
        self._Y_true_train['Normalized Emittance y'] *= 1000.*1000.
        self._Y_predict_train['Normalized Emittance x'] *= 1000.*1000.
        self._Y_predict_train['Normalized Emittance y'] *= 1000.*1000.

        # [dE] = keV
        self._y_diff_train['energy spread of the beam'] *= 1000.
        self._Y_true_train['energy spread of the beam'] *= 1000.
        self._Y_predict_train['energy spread of the beam'] *= 1000.


    def __init__(self,
                dvar_test, qoi_test,
                qoi_pred_test,
                plot_dir,
                model_name, experiment_name,
                dvar_train=None, qoi_train=None,
                qoi_pred_train=None,
                save_format='png', save_plots=True):
        self._X = dvar_test.drop(columns='sample_id').reset_index(drop=True)
        self._Y_true = qoi_test.astype('float64').reset_index(drop=True)
        self._Y_predict = qoi_pred_test
        
        if (dvar_train is not None) and (qoi_train is not None):
            self._X_train = dvar_train.drop(columns='sample_id').reset_index(drop=True)
            self._Y_true_train = qoi_train.astype('float64').reset_index(drop=True)
            self._Y_predict_train = qoi_pred_train

        self._plot_dir = plot_dir
        self._save_format = save_format
        self._save_plots = save_plots

        # calculate predictions and performance metrics
        self.init_predictions_test()
        if (dvar_train is not None) and (qoi_train is not None):
            self.init_predictions_train()

        self._symbols = {
            'Number of Macro Particles': 'N',
            'Mean Bunch Energy': 'E',
            'RMS Beamsize in x': r'\sigma_x',
            'RMS Beamsize in y': r'\sigma_y',
            'Normalized Emittance x': r'\epsilon_x',
            'Normalized Emittance y': r'\epsilon_y',
            'energy spread of the beam': r'\Delta E',
            'Correlation xpx': 'corr(x, p_x)',
            'Correlation ypy': 'corr(y, p_y)',
        }

        self._units = {
            'Number of Macro Particles': '',
            'Mean Bunch Energy': 'MeV',
            'RMS Beamsize in x': 'mm',
            'RMS Beamsize in y': 'mm',
            'Normalized Emittance x': 'mm mrad',
            'Normalized Emittance y': 'mm mrad',
            'energy spread of the beam': 'keV',
            'Correlation xpx': '',
            'Correlation ypy': '',
        }

        self._display_names = {
            'Number of Macro Particles': 'No. particles',
            'Mean Bunch Energy': 'E [MeV]',
            'RMS Beamsize in x': r'$\sigma_x$ [mm]',
            'RMS Beamsize in y': r'$\sigma_y$ [mm]',
            'Normalized Emittance x': r'$\epsilon_x$ [mm mrad]',
            'Normalized Emittance y': r'$\epsilon_y$ [mm mrad]',
            'energy spread of the beam': r'$\Delta E$ [keV]',
            'Correlation xpx': '$corr(x, p_x)$',
            'Correlation ypy': '$corr(y, p_y)$',
        }

        self._watermark_text = (f'Date: {str(datetime.date.today())}\n'
                          f'Model name: {model_name}\n'
                          f'Experiment name: {experiment_name}')

    @property
    def residuals_test(self):
        '''
        :returns: residuals over the test set; units: self._units
        '''
        return self._y_diff

    @property
    def residuals_train(self):
        '''
        :returns: residuals over the training set; units: self._units
        '''
        return self._y_diff_train

    @property
    def percentage_errors(self):
        return self._y_diff_rel_percentage

    @property
    def percentage_errors_train(self):
        return self._y_diff_rel_percentage_train

    def plot_residual_distribution(self, qoi_col, figsize=(10, 5), display_watermark=False):
        fig, ax = plt.subplots(figsize=figsize)

        sns.distplot(self._y_diff[qoi_col], ax=ax, axlabel=self._display_names[qoi_col])

        l = ax.get_xlabel()
        ax.set_xlabel(l, fontsize=34)
        ax.tick_params(labelsize=28)

        if display_watermark:
            fig.text(0, 0, s=self._watermark_text, color='gray')

        fig.tight_layout()
        if self._save_plots:
            fig.savefig(f'{self._plot_dir}/forward_prediction_error_distribution_{qoi_col}.{self._save_format}')

        return fig

    def plot_percentage_error_boxplots(self, qoi_col, cap_at_percentage=100, display_watermark=False):
        fig, ax = plt.subplots()

        sns.boxplot(self._y_diff_rel_percentage[qoi_col], ax=ax)
        ax.set_title(f'Relative error boxplot {qoi_col} (capped at {cap_at_percentage}%)')
        ax.set_xlabel('Rel. error [%]')
        ax.set_title(f'Relative error boxplot {qoi_col}')
        ax.set_xlabel('|Rel. error| [%]')
        ylim = ax.get_ylim()
        ylim[0] = max(ylim[0], -cap_at_percentage)
        ylim[1] = min(ylim[1], cap_at_percentage)
        ax.set_ylim(ylim)

        if display_watermark:
            fig.text(0, 0, s=self._watermark_text, color='gray')

        fig.tight_layout()
        if self._save_plots:
            fig.savefig(f'{self._plot_dir}/forward_prediction_abs_percentage_error_boxplot_{qoi_col}.{self._save_format}')

        return fig

    def plot_abs_percentage_error_boxplots(self, qoi_col, cap_at_percentage=100, display_watermark=False):
        fig, ax = plt.subplots()

        sns.boxplot(self._y_diff_rel_percentage[qoi_col].abs(), ax=ax)
        ax.set_title(f'|Relative error| boxplot {qoi_col} (capped at {cap_at_percentage}%)')
        ax.set_xlabel('|Rel. error| [%]')
        xlim = ax.get_xlim()
        xlim = list(xlim)
        xlim[1] = min(xlim[1], cap_at_percentage)
        ax.set_xlim(xlim)

        if display_watermark:
            fig.text(0, 0, s=self._watermark_text, color='gray')

        fig.tight_layout()
        if self._save_plots:
            fig.savefig(f'{self._plot_dir}/forward_prediction_abs_percentage_error_boxplot_{qoi_col}.{self._save_format}')

        return fig

    def plot_residual_distribution_at_YAG_screen(self, screen_position, YAG_id, logscale=False, display_watermark=False):
        # get position in the ground truth dataset that is closest to the YAG
        s_positions = self._X['Path length'].unique()
        closest_pos = s_positions[np.argmin(np.abs(s_positions - screen_position))]

        errors = self._y_diff[self._X['Path length'] == closest_pos]

        # determine layout of the subplots
        n_qoi = self._y_diff.shape[1]
        qoi_cols = 3
        qoi_rows = math.ceil(n_qoi / qoi_cols)

        fig, ax = plt.subplots(qoi_rows, qoi_cols, figsize=(22, 20))

        fig.suptitle(f'Error distribution for the prediction at {closest_pos:.3f}m (YAG{YAG_id}: {screen_position:.3f}m)',
                fontsize=40, fontweight='bold', y=1.)

        for i, col in enumerate(self._y_diff.columns):
            sns.distplot(errors[col], ax=ax.flatten()[i])

            ax.flatten()[i].tick_params(labelsize=28)
            ax.flatten()[i].set_xlabel(ax.flatten()[i].xaxis.get_label().get_text(), fontsize=34)
            if logscale:
                ax.flatten()[i].set_xscale('log')

        fig.tight_layout()
        fig.subplots_adjust(top=0.9)

        if self._save_plots:
            if logscale:
                fig.savefig(f'{self._plot_dir}/forward_prediction_error_log_distributions_at_YAG_{YAG_id}.{self._save_format}')
            else:
                fig.savefig(f'{self._plot_dir}/forward_prediction_error_distributions_at_YAG_{YAG_id}.{self._save_format}')

        return fig

    def boxplot_of_residuals(self, qoi_col, figsize=(10, 10)):
        fig, ax = plt.subplots(figsize=figsize)

        sns.boxplot(data=self._y_diff[qoi_col], ax=ax)

        ax.set_title(f'Error boxplot of ${self._symbols[qoi_col]}$', fontsize=34)
        ax.set_xticks([])
        ylabel = '${' + self._symbols[qoi_col] + r'}_\mathrm{true} - {' + self._symbols[qoi_col] + r'}_\mathrm{pred}$'
        if len(self._units[qoi_col]) > 0:
            ylabel += f' [{self._units[qoi_col]}]'
        print(ylabel)
        ax.set_ylabel(ylabel, fontsize=28);
        ax.tick_params(labelsize=28)

        fig.tight_layout()
        if self._save_plots:
            fig.savefig(f'{self._plot_dir}/forward_prediction_error_boxplot_N.{self._save_format}')

        return fig

    def plot_residual_vs_qoi(self, qoi_col, display_watermark=False):
        fig, ax = plt.subplots()
            
        ax.set_xlabel(self._display_names[qoi_col])
        if self._units[qoi_col]:
            ax.set_ylabel(f'Error [{self._units[qoi_col]}]')
        else:
            ax.set_ylabel('Error')

        ax.plot(self._Y_true[qoi_col], self._y_diff[qoi_col], 'x')

        if display_watermark:
            fig.text(0, 0, s=watermark_text, color='gray')

        fig.tight_layout()
        if self._save_plots:
            fig.savefig('{self._plot_dir}/forward_prediction_error_vs_value_{qoi_col}.{self._save_format}')
            
        return fig

    def plot_rel_error_vs_qoi(self, qoi_col, display_watermark=False, figsize=(10, 5)):
        fig, ax = plt.subplots(figsize=figsize)
            
        ax.set_xlabel(self._display_names[qoi_col], fontsize=34)
        ax.set_ylabel(f'|Rel. error| [%]', fontsize=34)

        ax.plot(self._Y_true[qoi_col], np.abs(self._y_diff_rel_percentage[qoi_col]), 'x')

        if display_watermark:
            fig.text(0, 0, s=self._watermark_text, color='gray')
                                        
        ax.tick_params(labelsize=28)

        fig.tight_layout()
        if self._save_plots:
            fig.savefig(f'{plot_dir}/forward_prediction_rel_error_vs_value_{qoi_col}.{self._save_format}')

        return fig

    def plot_rel_error_vs_qoi_each_sample(self, qoi_col):
        fig, ax = plt.subplots()

        ax.plot(self._Y_true[qoi_col], np.abs(self._y_diff_rel_percentage[qoi_col]),
                'x', label='test')
        ax.plot(self._Y_true_train[qoi_col], np.abs(self._y_diff_rel_train[qoi_col]),
                'x', label='train')

        ax.set_ylabel(r'$\left| \frac{y_\mathrm{true} - y_\mathrm{pred}}{y_\mathrm{true}} \right|$ [%]')
        xlabel = self._symbols[qoi_col]
        if self._units[qoi_col]:
            xlabel += f' [{self._units[qoi_col]}]'
        ax.set_xlabel(xlabel)

        fig.legend()
        fig.tight_layout()

        if self._save_plots:
            fig.savefig(f'{self._plot_dir}/forward_prediction_one_plot_{qoi_col}.{self._save_format}')

        return fig
        
    def plot_rel_error_vs_qoi_at_YAG(self, YAG_position, YAG_id):
        # select quantities at position of the YAG screen
        Y_train_YAG = self._Y_true_train[self._X_train['Path length'] == YAG_position]
        y_diff_rel_train_YAG = self._y_diff_rel_percentage_train[self._X_train['Path length'] == YAG_position]

        Y_test_YAG = self._Y_true[self._X['Path length'] == YAG_position]
        y_diff_rel_test_YAG = self._y_diff_rel_percentage[self._X['Path length'] == YAG_position]

        n_qoi = self._y_diff_rel_train.shape[1]
        qoi_cols = 3
        qoi_rows = math.ceil(n_qoi / qoi_cols)

        fig, axes = plt.subplots(qoi_rows, qoi_cols, figsize=(22, 20))

        fig.suptitle(f'Prediction error at YAG{YAG_id}: {YAG_position:.3f}m (capped at 200%)', fontsize=40, fontweight='bold', y=1.)

        for i, col in enumerate(self._Y_true_train.columns):
            # get the correct subplot
            ax = axes.flatten()[i]

            # plot
            if i == 0:
                train_label = 'train'
                test_label = 'test'
            else:
                train_label = ''
                test_label = ''
            ax.plot(Y_train_YAG[col], np.abs(y_diff_rel_train_YAG[col]), 'x', label=train_label)
            ax.plot(Y_test_YAG[col], np.abs(y_diff_rel_test_YAG[col]), 'x', label=test_label)

            # style the plots
            ax.set_xlabel(f'{self._display_names[col]}', fontsize=24)
            ax.set_ylim([0, 200])

            ax.tick_params(labelsize=20)
            
            # only show ylabel once per row at the very left
            if i % qoi_cols == 0:
                ax.set_ylabel(r'$\left| \frac{y_\mathrm{true} - y_\mathrm{pred}}{y_\mathrm{true}} \right|$ [%]', fontsize=24)

            fig.legend()
            fig.tight_layout()

        if self._save_plots:
            fig.savefig(f'{self._plot_dir}/forward_prediction_one_plot_at_YAG{YAG_id}.{self._save_format}')
        
        return fig

    def scatterplot_percentage_error_along_s(self, qoi, sample_size=500_000):
        plot_test = self._y_diff_rel_percentage.abs()
        plot_test['Path length'] = self._X['Path length']
        plot_test['kind'] = 'test'

        plot_train = self._y_diff_rel_percentage_train.abs()
        plot_train['Path length'] = self._X_train['Path length']
        plot_train['kind'] = 'train'

        to_plot = pd.concat([plot_test, plot_train], ignore_index=True)

        fig, ax = plt.subplots()

        sns.scatterplot(data=to_plot.sample(sample_size),
                        x='Path length',
                        y=qoi,
                        hue='kind',
                        hue_order=['train', 'test'],
                        s=10,
                        ax=ax)
        ax.set_ylim([0, 200]);
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)

        # the following two lines are taken from:
        # https://stackoverflow.com/a/47335905
        handles, labels = ax.get_legend_handles_labels()
        ax.legend(handles=handles[1:], labels=labels[1:])
        ax.set_title(f'Percentage errors (capped at 200%; sample of size {sample_size // 1000}k)')
        ax.grid(True, axis='both')

        if self._save_plots:
            fig.savefig(f'{self._plot_dir}/scatterplot_percentage_error_along_s_{qoi}.{self._save_format}')

        return fig
