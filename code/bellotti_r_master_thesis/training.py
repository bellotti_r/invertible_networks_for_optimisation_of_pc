import tensorflow as tf
from tensorflow import keras
from sklearn.model_selection import train_test_split

def build_model(model_parameters, number_of_inputs, number_of_outputs):
    loss = keras.losses.mean_absolute_error
    metrics = [keras.losses.mse, keras.losses.mean_absolute_percentage_error, keras.losses.mean_absolute_error]

    input_layer = keras.layers.Dense(input_shape=(number_of_inputs,), units=number_of_inputs)

    hidden_layers = []
    for i in range(model_parameters['hiddenLayers']):
        regulariser = None
        dropout = None
        activity_regularizer = None

        activation = model_parameters['activation']

        if activation == 'swish':
            activation = tf.nn.swish

        if 'l1_weight_regularisation' in model_parameters and\
            'l2_weight_regularisation' in model_parameters:
            raise RuntimeError('Specified two kinds of regularisation!')

        if 'l1_weight_regularisation' in model_parameters:
            regulariser = keras.regularizers.l1(model_parameters['l1_weight_regularisation'])
        elif 'l2_weight_regularisation' in model_parameters:
            regulariser = keras.regularizers.l2(model_parameters['l2_weight_regularisation'])
        elif 'dropout' in model_parameters:
            dropout = keras.layers.Dropout(rate=model_parameters['dropout'])
        elif ('l1_activity_regularization' in model_parameters) or ('l2_activity_regularization' in model_parameters):
            l1 = 0.
            l2 = 0.
            if 'l1_activity_regularization' in model_parameters:
                l1 = model_parameters['l1_activity_regularization']
            if 'l2_activity_regularization' in model_parameters:
                l2 = model_parameters['l2_activity_regularization']

            activity_regularizer = keras.layers.ActivityRegularization(l1=l1, l2=l2)

        layer = keras.layers.Dense(units=model_parameters['unitsPerLayer'],
                                   activation=activation,
                                   kernel_regularizer=regulariser)

        hidden_layers.append(layer)

        if dropout is not None:
            hidden_layers.append(dropout)

        if activity_regularizer is not None:
            hidden_layers.append(activity_regularizer)

    output_layer = keras.layers.Dense(units=number_of_outputs, activation='tanh')

    layers = [input_layer] + hidden_layers + [output_layer]

    model = keras.models.Sequential(layers)

    if model_parameters['optimizer'] == 'adam':
        optimizer = keras.optimizers.Adam(learning_rate=model_parameters['learning_rate'])
    elif model_parameters['optimizer'] == 'sgd':
        optimizer = keras.optimizers.SGD(learning_rate=model_parameters['learning_rate'])
    else:
        raise RuntimeError('Optimizer {} not implemented in this notebook!'.format(model_parameters['optimizer']))

    model.compile(
        loss=loss,
        optimizer=optimizer,
        metrics=metrics
    )

    return model


def train_entire_set_with_validation(model, X_train, y_train, X_val, y_val, batch_size, epochs, tensorboard_dir):
    callbacks = []

    if tensorboard_dir is not None:
        callbacks.append(keras.callbacks.TensorBoard(tensorboard_dir))

    if type(epochs) == str:
        # early stopping is used
        # the first number is the quantity to monitor,
        # the second one the patience,
        # the third one the minimum delta in the quantity to monitor,
        # and the fourth one the number of epochs
        monitor, patience, delta, epochs = epochs.split(',')
        patience = int(patience)
        delta = float(delta)
        epochs=int(epochs)

        early = keras.callbacks.EarlyStopping(monitor=monitor,
                                              min_delta=delta,
                                              patience=patience,
                                              mode='min')
        callbacks.append(early)

    model.fit(X_train, y_train,
            validation_data=(X_val, y_val),
            batch_size=batch_size,
            epochs=epochs,
            callbacks=callbacks,
            verbose=2); # don't print progress bar: speeds up training significantly!
    return model
