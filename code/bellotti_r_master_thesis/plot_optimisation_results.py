import numpy as np


class Ellipse:
    def __init__(self, x0, y0, a, b):
        self._x0 = x0
        self._y0 = y0
        self._a = a
        self._b = b

    def is_inside(self, x, y):
        return (((x - self._x0) / self._a)**2 + ((y - self._y0) / self._b)**2 <= 1.)


def draw_confidence_intervals(pred_x,
                              pred_y,
                              uncertainty_levels,
                              residuals_x,
                              residuals_y,
                              x_lim,
                              y_lim,
                              n_plot_dots,
                              colormap,
                              ax):
    '''
    Draws the confidence intervals for several confidence levels
    in a 2D scatterplot.

    Parameters
    ==========
    pred_x: 1-D array
        Values of the prediction along the x-axis
    pred_y: 1-D array
        Values of the prediction along the y-axis
    uncertainty_levels: 1-D array
        Confidence levels for which to draw the confidence intervals.
        For best results, use descending order for the values.
        That way, the smaller regions (lower confidence) are drawn on top of
        the bigger ones (higher confidence).
    residuals_x: 1-D array
        Uncertainty for the prediction along the x-axis at all levels.
        Must have the same shape as `uncertainty_levels`.
    residuals_y: 1-D array
        Uncertainty for the prediction along the y-axis at all levels.
        Must have the same shape as `uncertainty_levels`.
    x_lim: tuple
        Limit [a, b] for the x-axis. If `None`, use the limits of ax.
    y_lim: tuple
        Limit [a, b] for the y-axis. If `None`, use the limits of ax.
    n_plot_dots: int
        Number of points to use along each axis to plot
        the confidence intervals
    colormap: matplotlib.colors.Colormap
        Provides the colors in which the confidence intervals (CIs) are drawn.
        The first color is the one for the region outside the CIs.
    ax: matplotlib.axes.Axes
        Axis on which to draw the confidence intervals
    '''
    if x_lim is None:
        x_lim = ax.get_xlim()
    if y_lim is None:
        y_lim = ax.get_ylim()

    x_dots = np.linspace(*x_lim, n_plot_dots)
    y_dots = np.linspace(*y_lim, n_plot_dots)

    # everything that is not in the area of uncertainty is painted in the
    # background colour
    Z = np.zeros((n_plot_dots, n_plot_dots))

    for l, level in enumerate(uncertainty_levels):
        res_x = residuals_x[l]
        res_y = residuals_y[l]

        # lie ellipses around the predictions according to the uncertainties
        list_of_ellipses = []
        for i in range(len(pred_x)):
            ellipse = Ellipse(pred_x[i], pred_y[i], res_x, res_y)
            list_of_ellipses.append(ellipse)

        def in_ellipses_check(x_coord, y_coord):
            for ell in list_of_ellipses:
                if ell.is_inside(x_coord, y_coord):
                    return True
            return False

        # dye the "confidence interval" area for the current uncertainty level
        for j in range(n_plot_dots):
            for k in range(n_plot_dots):
                if in_ellipses_check(x_dots[j], y_dots[k]):
                    Z[j, k] = l + 1

    # plot
    xx, yy = np.meshgrid(x_dots, y_dots)
    ax.contourf(xx, yy, Z.T, zorder=0, cmap=colormap)
