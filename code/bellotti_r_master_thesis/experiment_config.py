import json
import numpy as np
import pandas as pd
from mllib.data import PandasHdfSource

pd.set_option('mode.chained_assignment', 'raise')


class ExperimentConfig:

    def __init__(self, path):
        with open(path, 'r') as file:
            content = json.load(file)

        self._content = content

        self._experiment_name = content['experiment_name']
        self._training_dataset_name = content['training_dataset_name']
        self._test_dataset_name = content['test_dataset_name']
        self._predicted_qois = content['predicted_qois']

        self._dvar_bounds = content['dvar_bounds']
        self._qoi_bounds = content['qoi_bounds']

        self._model_type = content['model_type']
        self._model_parameters = content['model_parameters']

        self._save_dir = content['save_dir']

        self._training_dvar = None
        self._training_qoi = None

        self._val_dvar = None
        self._val_qoi = None

        self._test_dvar = None
        self._test_qoi = None

    @property
    def experiment_name(self):
        return self._experiment_name

    @property
    def training_dataset_name(self):
        return self._training_dataset_name

    @property
    def test_dataset_name(self):
        return self._test_dataset_name

    @property
    def predicted_qois(self):
        return self._qois

    @property
    def dvar_bounds(self):
        return self._dvar_bounds

    @property
    def qoi_bounds(self):
        return self._qoi_bounds

    @property
    def model_type(self):
        return self._model_type

    @property
    def model_parameters(self):
        return self._model_parameters

    @property
    def save_dir(self):
        return self._save_dir

    def train_val_split(self, training_fraction=0.7):
        '''
        Split the training/validation data into a training and a validation set.

        The split is done by thresholding the sample_id values.
        Implicit assumption: OPAL does not have an order in the samples.

        :returns: dvar_train, qoi_train, dvar_val, qoi_val
        '''
        if self._training_dvar is None:
            self._load_train_val_data()

        # training/validation split
        dvar = self._training_dvar
        qoi = self._training_qoi

        num_OPAL_runs = len(dvar['sample_id'].unique())
        split_ID = int(num_OPAL_runs * training_fraction) + 1
        training_IDs = np.arange(split_ID)
        validation_IDs = np.arange(split_ID, num_OPAL_runs)

        dvar_train = dvar[dvar['sample_id'].isin(training_IDs)].drop(columns='sample_id')
        qoi_train = qoi[qoi['sample_id'].isin(training_IDs)]

        dvar_val = dvar[dvar['sample_id'].isin(validation_IDs)].drop(columns='sample_id')
        qoi_val = qoi[qoi['sample_id'].isin(validation_IDs)]

        dvar_train.reset_index(drop=True, inplace=True)
        dvar_val.reset_index(drop=True, inplace=True)
        qoi_train.reset_index(drop=True, inplace=True)
        qoi_val.reset_index(drop=True, inplace=True)

        return dvar_train, qoi_train[self._predicted_qois].copy(), dvar_val, qoi_val[self._predicted_qois].copy()

    def _load_train_val_data(self):
        # load training/validation dvar and qoi
        dvar, qoi = self._load_dvar_and_qoi(
            self._training_dataset_name,
            self._content['training_data_paths']
        )

        dvar.reset_index(drop=True, inplace=True)
        qoi.reset_index(drop=True, inplace=True)

        self._training_dvar = dvar
        self._training_qoi = qoi

    def training_data(self):
        '''Returns the training/validation data.'''
        if self._training_dvar is None:
            self._load_train_val_data()

        return self._training_dvar, self._training_qoi[self._predicted_qois].copy()

    def test_data(self):
        if self._test_dvar is None:
            dvar, qoi = self._load_dvar_and_qoi(
                self._test_dataset_name,
                self._content['test_data_paths']
            )

            dvar.reset_index(drop=True, inplace=True)
            qoi.reset_index(drop=True, inplace=True)

            self._test_dvar = dvar
            self._test_qoi = qoi

        return self._test_dvar, self._test_qoi[self._predicted_qois].copy()

    def _load_dvar_and_qoi(self, dataset_name, files):
        '''
        Find out if the data in the given file(s) is saved in one HDF file
        or two .npy files, load and return them
        '''
        if len(files) == 1:
            file = files[0]
            source = PandasHdfSource(dataset_name, file)
            dvar, qoi = source.get_data()
        elif len(files) == 2:
            qoi_path = None
            dvar_path = None

            for file in files:
                if file.endswith('_qoi.npy'):
                    qoi_path = file
                elif file.endswith('_dvar.npy'):
                    dvar_path = file

            # check naming conventions of the files
            if (qoi_path is None) or (dvar_path is None):
                raise RuntimeError(
                    'One of the data files must end in "_qoi.npy" and \
                    the other one in "_dvar.npy"'
                )

            # load the files
            dvar = np.load(dvar_path)
            qoi = np.load(qoi_path)
        else:
            raise RuntimeError('Give either 1 HDF file or 2 .npy files as data_files!')

        return dvar, qoi
