import numpy as np
import pandas as pd
from pymoo.model.problem import Problem
from pymoo.algorithms.nsga2 import NSGA2
from pymoo.factory import get_termination
from pymoo.optimize import minimize
from pymoo.util.nds.non_dominated_sorting import NonDominatedSorting


class SurrogateEvaluator:
    '''
    Class to collect multiple configurations to evaluate with a forward model.

    This allows to make use of the efficient TensorFlow implementation
    (pipelining, vectorisation, ...).
    '''

    def __init__(self, surrogate, qois):
        '''
        Parameters
        ==========
        surrogate:
            MLLIB model to be used for the evaluation.
        qois:
            List of the column names of the model output.
        '''
        self._configurations_to_evaluate = []
        self._surr = surrogate
        self._qois = qois

    def add_configuration(self, x, s):
        '''
        Parameters
        ==========
        x: 1D array
            Configuration of design variables to evaluate.
        s: float
            Position at which to evaluate the beam.
        '''
        input_vector = np.append(x, s*np.ones((x.shape[0], 1)), axis=1)
        self._configurations_to_evaluate.append(input_vector)

    def evaluate(self, batch_size=256):
        '''Actually perform the predictions.'''
        to_eval = np.vstack(self._configurations_to_evaluate)
        prediction = self._surr.predict(to_eval, batch_size=batch_size)

        return pd.DataFrame(data=prediction, columns=self._qois)


class AwaProblem(Problem):
    '''
    Class implementing the AWA optimisation problem so that it can be solved
    using the pymoo library.
    '''

    def __init__(self,
                 surrogate,
                 surr_qois,
                 corr_as_obj,
                 corr_as_constr):
        '''
        Parameters
        ==========
        surrogate: mllib.model.surrogate.Surrogate
            Surrogate model to use for the evaluation of the design variables.
        surr_qois: list of str
            Names of the quantities of interest that are the output
            of the model
        corr_as_obj: bool
            Whether |Corr(x, px)| at 26m is an objective
        corr_as_constr: bool
            Whether |Corr(x, px)| at 26m is a constraint
        '''
        self._surr = surrogate
        self._surr_qois = surr_qois
        self._corr_as_obj = corr_as_obj
        self._corr_as_constr = corr_as_constr

        # where to apply the constraints
        self._s_constraint = np.arange(0., 10., 0.25)

        self._beamsize_lower_bound = 2.  # mm
        self._beamsize_upper_bound = 5.  # mm

        self.lower_dvar_bound = [450., 150., -50.,   0.,   0.,   0., 0.3,  0.3,  1.5]
        self.upper_dvar_bound = [550., 260.,  10., 250., 200., 200., 5.0,  2.0, 12.5]

        # setup the pymoo problem
        n_constr = 2*len(self._s_constraint)
        if corr_as_constr:
            n_constr += 1

        n_obj = 3
        if corr_as_obj:
            n_obj += 1

        super().__init__(
            n_var=9,
            n_obj=n_obj,
            n_constr=n_constr,
            xl=self.lower_dvar_bound,
            xu=self.upper_dvar_bound
        )

    def _evaluate(self, x, out, *args, **kwargs):
        # use a class that collects all the necessary evalations
        # and evaluates them all at once in order to be faster
        evaluator = SurrogateEvaluator(self._surr, self._surr_qois)

        # evaluate constraints
        for s in self._s_constraint:
            evaluator.add_configuration(x, s)

        evaluator.add_configuration(x, 26.)
        all_predictions = evaluator.evaluate()

        all_predictions[['RMS Beamsize in x', 'RMS Beamsize in y']] *= 1000.
        all_predictions[['Normalized Emittance x', 'Normalized Emittance y']] *= 1e6
        all_predictions['energy spread of the beam'] *= 1000.

        # separate the predictions by their corresponding position
        # The output is ordered like this:
        # QOI corresponding to DVAR:        position:
        # x_0                               s_0
        # x_1                               s_0
        # ...                               ...
        # x_n_individuals                   s_0
        # x_0                               s_1
        # x_1                               s_1
        # ...                               ...
        predictions = all_predictions.iloc[0:self._s_constraint.shape[0]*x.shape[0], :]
        predictions_at_end = all_predictions.iloc[self._s_constraint.shape[0]*x.shape[0]:, :].copy()

        # calculate the constraints
        constraints_upper_bound = predictions['RMS Beamsize in x'] - self._beamsize_upper_bound
        constraints_lower_bound = self._beamsize_lower_bound - predictions['RMS Beamsize in x']

        # split the constraints so that each one is an element of a list
        constraints_upper_bound = np.split(constraints_upper_bound.values,
                                           len(self._s_constraint),
                                           axis=0)
        constraints_lower_bound = np.split(constraints_lower_bound.values,
                                           len(self._s_constraint),
                                           axis=0)

        # use each constraint as a column
        constraints = constraints_lower_bound + constraints_upper_bound

        if self._corr_as_constr:
            corr_constr = predictions_at_end['Correlation xpx'].abs() - 0.1
            constraints += [corr_constr.values]

        out['G'] = np.column_stack(constraints)

        # log the correlations (without absolute value)
        out['corr'] = predictions_at_end['Correlation xpx']

        # evaluate the objectives
        if self._corr_as_obj:
            predictions_at_end['Correlation xpx'] = predictions_at_end['Correlation xpx'].abs()
            objectives = predictions_at_end[[
                'RMS Beamsize in x',
                'Normalized Emittance x',
                'energy spread of the beam',
                'Correlation xpx',
            ]].values
        else:
            objectives = predictions_at_end[[
                'RMS Beamsize in x',
                'Normalized Emittance x',
                'energy spread of the beam',
            ]].values

        out['F'] = objectives

    def __getstate__(self):
        state = super().__getstate__().copy()
        del state['_surr']
        return state


class OptimisationRunner:
    '''
    Takes care of running an optimisation.

    This class handles the initialisation of the pymoo library, extracts the
    results of the optimisation into pandas DataFrames and numpy arrays,
    and saves them all to a single directory.
    '''

    # some constants
    awa_dvars = [
        'IBF',
        'IM',
        'GPHASE',
        'ILS1',
        'ILS2',
        'ILS3',
        'bunchCharge',
        'lambda',
        'SIGXY'
    ]

    awa_qois = [
        'Mean Bunch Energy',
        'RMS Beamsize in x',
        'RMS Beamsize in y',
        'Normalized Emittance x',
        'Normalized Emittance y',
        'energy spread of the beam',
        'Correlation xpx',
        'Correlation ypy'
    ]

    def __init__(self,
                 awa_or_isodar,
                 surr,
                 inv_surr=None,
                 target_for_INN=None):
        '''
        Parameters
        ==========
        awa_or_isodar: str
            Name of the problem to solve. Can be 'awa' or 'isodar'.
        surr: mllib.model.surrogate.Surrogate
            Surrogate model to use for the evaluation of the design variables.
        inv_surr: bellotti_r_master_thesis.invertible_network.invertible_neural_network.InvertibleNetworkSurrogate
            Surrogate model to use for initialisation of the first generation.
            If None, random initialisation is used.
        target_for_INN: pandas.DataFrame
            The target QOI vector to use for the biased initialisation.
            The INN will sample the first generation of individuals based on
            this vector.
        '''
        assert (awa_or_isodar == 'awa') or (awa_or_isodar == 'isodar')

        self._awa_or_isodar = awa_or_isodar
        self._surr = surr
        self._inv_surr = inv_surr
        self._inn_target_vector = target_for_INN

        # build a list of objectives
        if awa_or_isodar == 'awa':
            self._objectives = [
                'RMS Beamsize in x',
                'Normalized Emittance x',
                'energy spread of the beam'
            ]

            self._dvars = self.awa_dvars
            self._qois = self.awa_qois
        else:
            raise NotImplementedError

    def solve(self, n_gen, n_individuals):
        '''
        Parameters
        ==========
        n_gen: int
            number of generations
        n_individuals: int
            number of individuals in each generation
        '''
        # build the problem
        if self._awa_or_isodar == 'awa':
            problem = AwaProblem(self._surr,
                                 self._qois,
                                 False,
                                 True)
        else:
            raise NotImplementedError

        # initialise the algorithm
        if self._inv_surr is not None:
            # replicate the target vector many times in order to
            # sample n_individuals configurations, i. e. an entire generation
            target = [self._inn_target_vector.values] * n_individuals
            target_replicated = np.vstack(target)

            initial_configs = self._inv_surr.sample_n_tries(target_replicated,
                                                            n_tries=32,
                                                            batch_size=256)
            if self._awa_or_isodar == 'awa':
                cols = self._dvars + ['Path length']
                initial_configs = pd.DataFrame(data=initial_configs,
                                               columns=cols)
                initial_configs.drop(columns='Path length', inplace=True)
            else:
                raise NotImplementedError

            algorithm = NSGA2(
                pop_size=n_individuals,
                n_offsprings=n_individuals,
                sampling=initial_configs.values,
                eliminate_duplicates=True,
            )
        else:
            algorithm = NSGA2(
                pop_size=n_individuals,
                n_offsprings=n_individuals,
                eliminate_duplicates=True,
            )

        termination = get_termination("n_gen", n_gen)

        # solve
        res = minimize(problem,
                       algorithm,
                       termination,
                       seed=1,
                       pf=None,
                       save_history=True,
                       verbose=True)
        return res

    def extract_results(self, res):
        '''
        Parse the output of the pymoo optimisation and return the contents
        as pandas.DataFrame and numpy.array objects.

        Parameters
        ==========
        res: pymoo.model.result.Result
            Result of the optimisation from which to extract the data.

        Returns
        =======
        dict
            Available keys:
            - 'all_dvars'
            - 'all_objectives'
            - 'all_constraint_violations'
            - 'optimal_dvars'
            - 'optimal_objectives'
            - 'feasible'

            Shape of 'all_constraint_violations': (n_gen, n_individuals)
            All the other entries are pandas.DataFrame objects.
            The first two contain all DVARs and objectives for each generation.
            The last two contain only the ones of the non-dominated set.
        '''
        all_dvars = []
        all_objectives = []
        all_constraint_violations = []

        for gen, alg in enumerate(res.history):
            dvar_in_gen = pd.DataFrame(data=alg.pop.get('X'),
                                       columns=self._dvars)
            dvar_in_gen['generation'] = gen
            all_dvars.append(dvar_in_gen)

            obj = np.append(alg.pop.get('F'),
                            alg.pop.get('corr'),
                            axis=1)
            cols = self._objectives + ['Correlation xpx']
            obj_in_gen = pd.DataFrame(data=obj,
                                      columns=cols)
            obj_in_gen['generation'] = gen
            all_objectives.append(obj_in_gen)

            cv_in_gen = alg.pop.get('CV')
            cv_in_gen = cv_in_gen.reshape((1, obj_in_gen.shape[0]))
            all_constraint_violations.append(cv_in_gen)

        all_dvars = pd.concat(all_dvars, ignore_index=True)
        all_objectives = pd.concat(all_objectives, ignore_index=True)

        all_constraint_violations = np.vstack(all_constraint_violations)

        # non-dominated solutions
        feasible_indices_per_gen = [alg.pop.get('feasible')[:, 0] for alg in res.history]
        number_feasible_per_gen = [len(i) for i in feasible_indices_per_gen]
        number_feasible = sum(number_feasible_per_gen)

        if number_feasible == 0:
            # feasible = False
            #
            # to_sort_all_qois = all_objectives.copy()
            # to_sort = all_objectives.drop(columns=[
            #     'Correlation xpx',
            #     'generation',
            # ]).values
            # dvars = all_dvars.copy()
            raise RuntimeError('No feasible individuals found!!!')
        else:
            feasible = True
            feasible_populations = [alg.pop[alg.pop.get('feasible')[:, 0]] for alg in res.history]

            tmp_obj = []
            tmp_dvar = []
            for pop in feasible_populations:
                if pop.get('F').shape[0] == 0:
                    # no feasible solution in this generation
                    continue
                else:
                    if len(pop.get('F').shape) == 1:
                        to_add = np.append(pop.get('F'),
                                           pop.get('corr'))
                        to_add = np.reshape((1, -1))
                        dvar = pop.get('X').reshape((1, -1))
                    else:
                        to_add = np.append(pop.get('F'),
                                           pop.get('corr'),
                                           axis=1)
                        dvar = pop.get('X')
                    tmp_obj.append(to_add)
                    tmp_dvar.append(dvar)

            dvars = pd.DataFrame(data=np.vstack(tmp_dvar),
                                 columns=all_dvars.drop(columns='generation').columns)

            cols = all_objectives.drop(columns='generation').columns
            to_sort_all_qois = pd.DataFrame(data=np.vstack(tmp_obj),
                                            columns=cols)

            # Some individuals show up in multiple generations.
            # To avoid double-counting, we remove the duplicates.
            dvars.drop_duplicates(inplace=True)
            to_sort_all_qois = to_sort_all_qois.loc[dvars.index]

            dvars.reset_index(drop=True, inplace=True)
            to_sort_all_qois.reset_index(drop=True, inplace=True)

            to_sort = to_sort_all_qois.drop(columns='Correlation xpx').values

        nds = NonDominatedSorting()
        optimal_indices = nds.do(to_sort,
                                 only_non_dominated_front=True)

        optimal_dvars = dvars.loc[optimal_indices, :].reset_index(drop=True)
        optimal_objectives = to_sort_all_qois.loc[optimal_indices, :].reset_index(drop=True)

        return {
            'all_dvars': all_dvars,
            'all_objectives': all_objectives,
            'all_constraint_violations': all_constraint_violations,
            'optimal_dvars': optimal_dvars,
            'optimal_objectives': optimal_objectives,
            'feasible': feasible,
        }

    @staticmethod
    def save_results(output_dir, result):
        '''Saves the results to a single output directory.'''
        all_dvars = result['all_dvars']
        all_objectives = result['all_objectives']
        all_constraint_violations = result['all_constraint_violations']
        optimal_dvars = result['optimal_dvars']
        optimal_objectives = result['optimal_objectives']

        # generations
        all_dvars.to_hdf(f'{output_dir}/dvar_generations.hdf5', key='dvar')
        all_dvars.to_csv(f'{output_dir}/dvar_generations.csv')
        all_objectives.to_hdf(f'{output_dir}/objective_generations.hdf5',
                              key='qoi')
        all_objectives.to_csv(f'{output_dir}/objective_generations.csv')

        # non-dominated set
        optimal_dvars.to_csv(f'{output_dir}/optimal_dvars.tsv',
                             index=False,
                             sep='\t')
        optimal_dvars.to_hdf(f'{output_dir}/optimal_dvars.hdf5', key='dvar')
        optimal_objectives.to_hdf(f'{output_dir}/optimal_qois.hdf5', key='obj')

        # constraint violation
        # Each row contains the constraint violations of the individuals of
        # one generation. The columns correspond to the individuals
        np.save(f'{output_dir}/constraint_violations.npy',
                all_constraint_violations)
