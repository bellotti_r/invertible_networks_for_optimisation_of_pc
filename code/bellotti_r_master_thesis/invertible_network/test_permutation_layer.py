import os
import unittest
import numpy as np
import tensorflow as tf
from permutation_layer import PermutationLayer


class PermutationLayerTest(unittest.TestCase):
    def setUp(self):
        # reproducibility
        np.random.seed(287999829)

        self.shape = (5, 6)
        self.x_test = np.arange(self.shape[0]*self.shape[1]).reshape(self.shape)

        self.save_file = 'test_model.hdf5'
        if os.path.exists(self.save_file):
            os.remove(self.save_file)

    def testForward(self):
        # build the simplest network consisting of a PermutationLayer only
        layer = tf.keras.layers.Input((self.shape[1],))
        l1 = PermutationLayer()
        m = tf.keras.models.Sequential([layer, l1])

        # permute the columns of x_test
        permuted = m(self.x_test)

        # check shapes
        self.assertTrue(permuted.shape == self.x_test.shape)

        # check if all obtained columns are columns of x_test
        true_columns = np.hsplit(self.x_test, self.shape[1])
        permuted_columns = np.hsplit(permuted, self.shape[1])

        for col in permuted_columns:
            equalities = [np.isclose(col, other).all() for other in true_columns]
            num_true = equalities.count(True)
            self.assertTrue(num_true == 1)

        # check if forward and backward passes lead to the same result
        inverse_permuted = l1.inverse(permuted)
        self.assertTrue(tf.reduce_all(tf.math.equal(self.x_test, inverse_permuted)))

    def test_save_load(self):
        layer = tf.keras.layers.Input((self.shape[1],))
        l1 = PermutationLayer()
        model_1 = tf.keras.models.Sequential([layer, l1])
        model_1.save(self.save_file)

        model_2 = tf.keras.models.load_model(self.save_file,
                                             custom_objects={
                                                'PermutationLayer': PermutationLayer
                                             })

        pred_1 = model_1.predict(self.x_test)
        pred_2 = model_2.predict(self.x_test)

        self.assertTrue(np.isclose(pred_1, pred_2).all())

    def test_reproducibility(self):
        SEED = 287999829
        np.random.seed(SEED)
        tf.random.set_seed(SEED)

        layer = tf.keras.layers.Input((self.shape[1],))
        l1 = PermutationLayer()
        model_1 = tf.keras.models.Sequential([layer, l1])
        pred_1 = model_1.predict(self.x_test)

        # if the seed is reset, the predictions should yield the same
        np.random.seed(SEED)
        tf.random.set_seed(SEED)

        layer = tf.keras.layers.Input((self.shape[1],))
        l1 = PermutationLayer()
        model_2 = tf.keras.models.Sequential([layer, l1])
        pred_2 = model_2.predict(self.x_test)

        self.assertTrue(np.isclose(pred_1, pred_2).all())

        # if the seed is not the same, the predictions should be different
        layer = tf.keras.layers.Input((self.shape[1],))
        l1 = PermutationLayer()
        model_3 = tf.keras.models.Sequential([layer, l1])
        pred_3 = model_3.predict(self.x_test)

        self.assertFalse(np.isclose(pred_1, pred_3).all())


if __name__ == '__main__':
    unittest.main()
