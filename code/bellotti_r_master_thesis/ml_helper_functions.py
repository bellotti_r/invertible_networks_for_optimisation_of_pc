import pandas as pd
import tensorflow as tf


def load_dataset(dataset_name, dataset_dir='datasets', basename='awa'):
    '''
    Returns design variables X, quantities of interest Y for the given dataset.

    :param dataset_name: name of the dataset
    :param dataset_dir: directory where all the datasets are stored (in subdirectories)
    :param basename: the basename of the HDF5 files inside the subdirectory corresponding to the given dataset
    '''
    dataset_path = '{}/{}/{}_dataset.hdf5'.format(dataset_dir, dataset_name, basename)

    dvar = pd.read_hdf(dataset_path, key='dvar', mode='r')
    qoi = pd.read_hdf(dataset_path, key='qoi', mode='r')

    return dvar, qoi

# +
default_columns_to_keep = [
    'Number of Macro Particles',
    'Mean Bunch Energy',
    'RMS Beamsize in x',
    'RMS Beamsize in y',
    #'RMS Beamsize in s',
    'Normalized Emittance x',
    'Normalized Emittance y',
    #'Normalized Emittance s',
    #'RMS Normalized Momenta in x',
    #'RMS Normalized Momenta in y',
    #'RMS Normalized Momenta in s',
    'energy spread of the beam',
    'Correlation xpx',
    'Correlation ypy',
    #'Correlation zpz',
]

default_columns_to_keep_no_N = [
    'Mean Bunch Energy',
    'RMS Beamsize in x',
    'RMS Beamsize in y',
    'RMS Beamsize in s',
    'Normalized Emittance x',
    'Normalized Emittance y',
    'Normalized Emittance s',
    'energy spread of the beam'
]

default_columns_to_keep_including_momenta = [
    'Number of Macro Particles',
    'Mean Bunch Energy',
    'RMS Beamsize in x',
    'RMS Beamsize in y',
    'RMS Beamsize in s',
    'RMS Normalized Momenta in x',
    'RMS Normalized Momenta in y',
    'RMS Normalized Momenta in s',
    'Normalized Emittance x',
    'Normalized Emittance y',
    'Normalized Emittance s',
    'energy spread of the beam'
]
# -

dvar_mapper = {
    'IBF': 'IBF [A]',
    'IM': 'IM [A]',
    'GPHASE': 'GPHASE [deg]',
    'FWHM': 'FWHM [s]',
    'KQ1': 'KQ1 [$m^{-1}$]',
    'KQ2': 'KQ2 [$m^{-1}$]',
    'KQ3': 'KQ3 [$m^{-1}$]',
    'KQ4': 'KQ4 [$m^{-1}$]',
}

qoi_mapper = {
    'Number of Macro Particles': '# Particles',
    'Mean Bunch Energy': 'Mean Bunch Energy [MeV]',
    'RMS Beamsize in x': 'RMS Beamsize x [m]',
    'RMS Beamsize in y': 'RMS Beamsize y [m]',
    'RMS Beamsize in s': 'RMS Beamsize s [m]',
    'Normalized Emittance x': 'Normalized $\epsilon_x$ [m]',
    'Normalized Emittance y': 'Normalized $\epsilon_y$ [m]',
    'Normalized Emittance s': 'Normalized $\epsilon_s$ [m]',
    'energy spread of the beam': 'energy spread of the beam [MeV]'
}

column_indices_to_keep = {
    'Number of Macro Particles': 1,
    'Mean Bunch Energy': 3,
    'RMS Beamsize in x': 4,
    'RMS Beamsize in y': 5,
    'RMS Beamsize in s': 6,
    'Normalized Emittance x': 10,
    'Normalized Emittance y': 11,
    'Normalized Emittance s': 12,
    'energy spread of the beam': 38,
}

# +
column_indices_of_interest = [
    1,  # Number of Macro Particles
    3,  # Mean Bunch Energy
    4,  # RMS Beamsize in x
    5,  # RMS Beamsize in y
    6,  # RMS Beamsize in s
    10, # Normalized Emittance x
    11, # Normalized Emittance y
    12, # Normalized Emittance s'
    38, # energy spread of the beam
]

column_of_interest_labels = [
    'Number of Macro Particles',
    'Mean Bunch Energy',
    'RMS Beamsize in x',
    'RMS Beamsize in y',
    'RMS Beamsize in s',
    'Normalized Emittance x',
    'Normalized Emittance y',
    'Normalized Emittance s',
    'energy spread of the beam',
]


# +
dvar_columns = [
    'IBF',
    'IM',
    'GPHASE',
    'FWHM',
    'KQ1',
    'KQ2',
    'KQ3',
    'KQ4',
    'Path length'
]

all_qoi_columns = [
    'Time',
    'Number of Macro Particles',
    'Bunch Charge',
    'Mean Bunch Energy',
    'RMS Beamsize in x',
    'RMS Beamsize in y',
    'RMS Beamsize in s',
    'RMS Normalized Momenta in x',
    'RMS Normalized Momenta in y',
    'RMS Normalized Momenta in s',
    'Normalized Emittance x',
    'Normalized Emittance y',
    'Normalized Emittance s',
    'Mean Beam Position in x',
    'Mean Beam Position in y',
    'Mean Beam Position in s',
    'x coordinate of reference particle in lab cs',
    'y coordinate of reference particle in lab cs',
    'z coordinate of reference particle in lab cs',
    'x momentum of reference particle in lab cs',
    'y momentum of reference particle in lab cs',
    'z momentum of reference particle in lab cs',
    'Max Beamsize in x',
    'Max Beamsize in y',
    'Max Beamsize in s',
    'Correlation xpx',
    'Correlation ypy',
    'Correlation zpz',
    'Dispersion in x',
    'Derivative of dispersion in x',
    'Dispersion in y',
    'Derivative of dispersion in y',
    'Bx-Field component of ref particle',
    'By-Field component of ref particle',
    'Bz-Field component of ref particle',
    'Ex-Field component of ref particle',
    'Ey-Field component of ref particle',
    'Ez-Field component of ref particle',
    'energy spread of the beam',
    'time step size',
    'outside n*sigma of the beam',
]


# -

def select_columns_of_interest(qoi, column_indices_to_keep=default_columns_to_keep):
    Y = qoi[column_indices_to_keep]
    return Y

# unit: m
YAG_screen_positions = [
    0.5,
    2.93,
    6.22,
    9.47,
    11.36,
    11.57,
    15.13,
    16.70,
    19.26,
    20.69,
    22.99,
    25.27,
    #27.78
]


all_cavity_locations = [
    (0.65, 1.71),
    (3.39, 4.44),
    (4.94, 5.99),
    (7.08, 8.13),
    (8.42, 9.47),
    (9.83, 10.89),
]

cavity_locations = [
    (0.65, 1.71),
    (3.39, 4.44),
    (4.94, 5.99),
    (8.42, 9.47),
]

half_solenoid_width = 0.2

solenoid_locations = [
    (0, 0.4),
    (2.08 - half_solenoid_width, 2.08 + half_solenoid_width),
    (4.65 - half_solenoid_width, 4.65 + half_solenoid_width),
    (6.69 - half_solenoid_width, 6.69 + half_solenoid_width),
]

# WARNING:
# Not the order in which the forward surrogate model outputs its predictions!!!
qoi_columns = [
    'RMS Beamsize in x',
    'RMS Beamsize in y',
    'Normalized Emittance x',
    'Normalized Emittance y',
    'Mean Bunch Energy',
    'energy spread of the beam',
    'Correlation xpx',
    'Correlation ypy',
]


class RSquaredSeparated(tf.keras.losses.Loss):
    '''
    Calculates the R^2 value for each predicted quantity separately.

    For more details, see:
    https://www.analyticsvidhya.com/blog/2020/07/difference-between-r-squared-and-adjusted-r-squared/
    '''
    def __init__(self):
        super().__init__(name='r2')

    def call(self, y_true, y_pred):
        mean_true = tf.math.reduce_mean(y_true, axis=0)

        total_sum_of_squares = tf.math.reduce_sum(tf.math.squared_difference(y_true, mean_true),
                                                  axis=0)
        residual_sum_of_squares = tf.math.reduce_sum(tf.math.squared_difference(y_true, y_pred),
                                                     axis=0)
        r2 = 1. - residual_sum_of_squares / total_sum_of_squares

        return r2


class AdjustedRSquaredSeparated(tf.keras.losses.Loss):
    '''
    Calculates the adjusted R^2 value for each predicted quantity separately.

    For more details, see:
    https://www.analyticsvidhya.com/blog/2020/07/difference-between-r-squared-and-adjusted-r-squared/
    '''
    def __init__(self, batch_size, number_of_input):
        '''
        Parameters
        ==========
        batch_size: int
            Number of samples in a batch,
            i. e. number of rows of a batch of the X matrix.
        number_of_input: int
            Number of independent variables (=columns) in the problem,
            i. e. number of columns of the X matrix.
        '''
        super().__init__(name='adjusted_r2')
        self._n = batch_size
        self._n_in = number_of_input

    def call(self, y_true, y_pred):
        r2 = RSquaredSeparated().call(y_true, y_pred)

        adjusted_r2 = 1. - (1. - r2) * (self._n - 1.) / (self._n - self._n_in - 1.)

        return adjusted_r2

    @classmethod
    def from_config(cls, config):
        return AdjustedRSquaredSeparated(config['n'], config['n_in'])

    def get_config(self):
        return {
            'n': self._n,
            'n_in': self._n_in,
        }
