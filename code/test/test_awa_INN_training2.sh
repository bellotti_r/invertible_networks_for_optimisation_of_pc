#!/bin/bash
module use unstable
module load anaconda

conda activate /data/user/bellotti_r/turning_tables_env
python3 scripts/train_network_script.py experiment_configs/test_INN2.json
