import os
import shutil
import unittest
import numpy as np
import pandas as pd
import tensorflow as tf
from mllib.model import KerasSurrogate
from bellotti_r_master_thesis.awa_training import train_awa_forward_model,\
                                                  train_awa_invertible_model
from bellotti_r_master_thesis.invertible_network.invertible_neural_network import InvertibleNetworkSurrogate
tf.keras.backend.set_floatx('float64')


class AwaForwardTrainingTest(unittest.TestCase):

    def setUp(self):
        # reproducibility
        SEED = 9357
        tf.random.set_seed(SEED)
        np.random.seed(SEED)

        self.dvar_bounds = {
            'IBF': [450.0, 550.0],
            'IM': [100.0, 260.0],
            'GPHASE': [-50.0, 10.0],
            'ILS1': [0.0, 250.0],
            'ILS2': [0.0, 200.0],
            'ILS3': [0.0, 200.0],
            'bunchCharge': [0.3, 5.0],
            'lambda': [0.3, 2.0],
            'SIGXY': [1.5, 12.5],
            'Path length': [0.0, 26.0]
        }

        self.dvar = list(self.dvar_bounds.keys())

        self.qoi = [
            'Mean Bunch Energy',
            'RMS Beamsize in x',
            'RMS Beamsize in y',
            'Normalized Emittance x',
            'Normalized Emittance y',
            'energy spread of the beam',
            'Correlation xpx',
            'Correlation ypy',
        ]

        self.model_parameters = {
            'hiddenLayers': 3,
            'unitsPerLayer': 10,
            'activation': 'relu',
            'optimizer': 'adam',
            'learning_rate': 0.0001,
            'batch_size': 256,
            'epochs': 2,
        }

        self.tensorboard_dir = 'tensorboard_awa_training_forward_model'

        if os.path.exists(self.tensorboard_dir):
            shutil.rmtree(self.tensorboard_dir)
        os.makedirs(self.tensorboard_dir)

        self.save_dir = 'awa_forward_model'
        if os.path.exists(self.tensorboard_dir):
            shutil.rmtree(self.tensorboard_dir)
        os.makedirs(self.tensorboard_dir)

        self.version = f'TensorFlow version: {tf.__version__}'
        self.name = 'awa_test_model'

        # generate dummy data
        self.n_train = 5_000
        self.n_val = 1_000
        gen = np.random.Generator(np.random.PCG64(seed=48937))

        self.X_train = pd.DataFrame()
        for d in self.dvar:
            self.X_train[d] = gen.uniform(size=self.n_train,
                                          low=self.dvar_bounds[d][0],
                                          high=self.dvar_bounds[d][1])
        self.X_train = pd.DataFrame(data=self.X_train, columns=self.dvar)

        self.X_val = pd.DataFrame()
        for d in self.dvar:
            self.X_val[d] = gen.uniform(size=self.n_val,
                                        low=self.dvar_bounds[d][0],
                                        high=self.dvar_bounds[d][1])
        self.y_train = pd.DataFrame()
        for q in self.qoi:
            self.y_train[q] = gen.normal(size=self.n_train)

        self.y_val = pd.DataFrame()
        for q in self.qoi:
            self.y_val[q] = gen.normal(size=self.n_val)

    def test_input(self):
        self.assertTrue(self.X_train.shape == (self.n_train, len(self.dvar)))
        self.assertTrue((self.X_train.columns == self.dvar).all())

        self.assertTrue(self.X_val.shape == (self.n_val, len(self.dvar)))
        self.assertTrue((self.X_val.columns == self.dvar).all())

        self.assertTrue(self.y_train.shape == (self.n_train, len(self.qoi)))
        self.assertTrue((self.y_train.columns == self.qoi).all())

        self.assertTrue(self.y_val.shape == (self.n_val, len(self.qoi)))
        self.assertTrue((self.y_val.columns == self.qoi).all())

    def test_reproducility(self):
        SEED = 9357
        tf.random.set_seed(SEED)
        np.random.seed(SEED)

        surr_1 = train_awa_forward_model(self.dvar_bounds,
                                         self.X_train,
                                         self.y_train,
                                         self.X_val,
                                         self.y_val,
                                         self.model_parameters,
                                         self.tensorboard_dir,
                                         self.version,
                                         self.name + '_1')
        surr_1.save('.')
        tf.random.set_seed(SEED)
        np.random.seed(SEED)

        surr_2 = train_awa_forward_model(self.dvar_bounds,
                                         self.X_train,
                                         self.y_train,
                                         self.X_val,
                                         self.y_val,
                                         self.model_parameters,
                                         self.tensorboard_dir,
                                         self.version,
                                         self.name + '_2')
        surr_2.save('.')

        batch_size = self.model_parameters['batch_size']
        pred_1 = surr_1.predict(self.X_val.values, batch_size=batch_size)
        pred_2 = surr_2.predict(self.X_val.values, batch_size=batch_size)

        self.assertTrue(np.all(np.isclose(pred_1, pred_2)))

    def test_saving_and_loading(self):
        SEED = 9357
        tf.random.set_seed(SEED)
        np.random.seed(SEED)

        surr_1 = train_awa_forward_model(self.dvar_bounds,
                                         self.X_train,
                                         self.y_train,
                                         self.X_val,
                                         self.y_val,
                                         self.model_parameters,
                                         self.tensorboard_dir,
                                         self.version,
                                         self.name)
        surr_1.save(self.save_dir)

        surr_2 = KerasSurrogate.load(self.save_dir, surr_1.name)

        batch_size = self.model_parameters['batch_size']
        pred_1 = surr_1.predict(self.X_val.values, batch_size=batch_size)
        pred_2 = surr_2.predict(self.X_val.values, batch_size=batch_size)

        self.assertTrue(np.all(np.isclose(pred_1, pred_2)))


class AwaInvertibleTrainingTest(unittest.TestCase):

    def setUp(self):
        # reproducibility
        SEED = 9357
        tf.random.set_seed(SEED)
        np.random.seed(SEED)

        self.dvar_bounds = {
            'IBF': [450.0, 550.0],
            'IM': [100.0, 260.0],
            'GPHASE': [-50.0, 10.0],
            'ILS1': [0.0, 250.0],
            'ILS2': [0.0, 200.0],
            'ILS3': [0.0, 200.0],
            'bunchCharge': [0.3, 5.0],
            'lambda': [0.3, 2.0],
            'SIGXY': [1.5, 12.5],
            'Path length': [0.0, 26.0]
        }

        self.qoi_bounds = {
            'Mean Bunch Energy': [0.0, 70.0],
            'RMS Beamsize in x': [0.0, 2.05],
            'RMS Beamsize in y': [0.0, 2.05],
            'Normalized Emittance x': [0.0, 0.018],
            'Normalized Emittance y': [0.0, 0.018],
            'energy spread of the beam': [0.0, 32.0],
            'Correlation xpx': [-1.0, 1.0],
            'Correlation ypy': [-1.0, 1.0],
            'Path length': [0., 13.7],
        }

        self.dvar = list(self.dvar_bounds.keys())

        self.qoi = [
            'Mean Bunch Energy',
            'RMS Beamsize in x',
            'RMS Beamsize in y',
            'Normalized Emittance x',
            'Normalized Emittance y',
            'energy spread of the beam',
        ]

        self.tensorboard_dir = 'tensorboard_awa_training_invertible_model'

        if os.path.exists(self.tensorboard_dir):
            shutil.rmtree(self.tensorboard_dir)
        os.makedirs(self.tensorboard_dir)

        self.model_parameters = {
            'validation_model': ['/data/user/bellotti_r/paper_new/models',
                                 'awa_forward_model'],
            'num_dvars_to_average_for_validation': 1,
            'x_dim': 10,
            'y_dim': 7,
            'z_dim': 1,
            'nominal_dim': 12,
            'coefficient_net_activations': [
                'relu',
                'relu',
                'linear'
            ],
            'sampling_distribution': 'uniform',
            'loss_weight_artificial': 1.0,
            'loss_weight_reconstruction': 3.0,
            'optimizer': 'adam',
            'learning_rate': 0.0001,
            'epochs': 2,
            'batch_size': 256,
            'number_of_blocks': 2,
            'loss_weight_x': 400,
            'loss_weight_y': 400,
            'loss_weight_z': 400,
            'coefficient_net_units': [
                10,
                10,
                6
            ],
            'tensorboard_dir': self.tensorboard_dir,
        }

        self.save_dir = 'awa_invertible_model'
        if os.path.exists(self.save_dir):
            shutil.rmtree(self.save_dir)
        os.makedirs(self.save_dir)

        self.version = f'TensorFlow version: {tf.__version__}'
        self.name = 'awa_test_model'

        # generate dummy data
        self.n_train = 3_000
        self.n_val = 1_000
        gen = np.random.Generator(np.random.PCG64(seed=48937))

        self.X_train = pd.DataFrame()
        for d in self.dvar:
            self.X_train[d] = gen.uniform(size=self.n_train,
                                          low=self.dvar_bounds[d][0],
                                          high=self.dvar_bounds[d][1])
        self.X_train = pd.DataFrame(data=self.X_train, columns=self.dvar)

        self.X_val = pd.DataFrame()
        for d in self.dvar:
            self.X_val[d] = gen.uniform(size=self.n_val,
                                        low=self.dvar_bounds[d][0],
                                        high=self.dvar_bounds[d][1])
        self.y_train = pd.DataFrame()
        for q in self.qoi:
            self.y_train[q] = gen.normal(size=self.n_train)

        self.y_val = pd.DataFrame()
        for q in self.qoi:
            self.y_val[q] = gen.normal(size=self.n_val)

    def test_input(self):
        self.assertTrue(self.X_train.shape == (self.n_train, len(self.dvar)))
        self.assertTrue((self.X_train.columns == self.dvar).all())

        self.assertTrue(self.X_val.shape == (self.n_val, len(self.dvar)))
        self.assertTrue((self.X_val.columns == self.dvar).all())

        self.assertTrue(self.y_train.shape == (self.n_train, len(self.qoi)))
        self.assertTrue((self.y_train.columns == self.qoi).all())

        self.assertTrue(self.y_val.shape == (self.n_val, len(self.qoi)))
        self.assertTrue((self.y_val.columns == self.qoi).all())

    def test_reproducibility(self):
        SEED = 9357

        batch_size = self.model_parameters['batch_size']

        tf.random.set_seed(SEED)
        np.random.seed(SEED)

        surr_1 = train_awa_invertible_model(self.qoi_bounds,
                                            self.X_train,
                                            self.y_train,
                                            self.X_val,
                                            self.y_val,
                                            self.model_parameters,
                                            self.version,
                                            self.name + '_1')
        pred_1 = surr_1.predict(self.X_val.values, batch_size=batch_size)

        tf.random.set_seed(SEED)
        np.random.seed(SEED)

        surr_2 = train_awa_invertible_model(self.qoi_bounds,
                                            self.X_train,
                                            self.y_train,
                                            self.X_val,
                                            self.y_val,
                                            self.model_parameters,
                                            self.version,
                                            self.name + '_2')

        pred_2 = surr_2.predict(self.X_val.values, batch_size=batch_size)

        surr_1.save('reproducibility')
        surr_2.save('reproducibility')

        self.assertTrue(np.all(np.isclose(pred_1, pred_2)))

    def test_saving_and_loading(self):
        SEED = 9357
        tf.random.set_seed(SEED)
        np.random.seed(SEED)

        surr_1 = train_awa_invertible_model(self.qoi_bounds,
                                            self.X_train,
                                            self.y_train,
                                            self.X_val,
                                            self.y_val,
                                            self.model_parameters,
                                            self.version,
                                            self.name)
        surr_1.save(self.save_dir)

        surr_2 = InvertibleNetworkSurrogate.load(self.save_dir, surr_1.name)

        batch_size = self.model_parameters['batch_size']

        tf.random.set_seed(SEED)
        np.random.seed(SEED)

        pred_1 = surr_1.predict(self.X_val.values, batch_size=batch_size)

        tf.random.set_seed(SEED)
        np.random.seed(SEED)

        pred_2 = surr_2.predict(self.X_val.values, batch_size=batch_size)

        self.assertTrue(np.all(np.isclose(pred_1, pred_2)))


if __name__ == '__main__':
    unittest.main()
